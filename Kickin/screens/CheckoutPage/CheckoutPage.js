import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, ActivityIndicator} from 'react-native';
import Checkout from '../../component/Checkout';
import {useDispatch, useSelector} from 'react-redux';
import {getCheckout} from '../../redux/Action/BookingAction';
import {Icon} from 'react-native-elements';
import {useFocusEffect} from '@react-navigation/native';

export default function CheckoutPage({navigation}) {
  const dispatch = useDispatch();
  const bookingState = useSelector((state) => state.booking);

  useFocusEffect(
    React.useCallback(() => {
      dispatch(getCheckout());
    }, [dispatch]),
  );

  const checkout = bookingState.checkout;

  return (
    <View style={styles.container}>
      {bookingState.isLoading ? (
        <View style={styles.loadingScreen}>
          <ActivityIndicator size="large" color="#53C9C2" />
        </View>
      ) : checkout.length === 0 ? (
        <View style={styles.noData}>
          <Icon
            name="box-open"
            type="font-awesome-5"
            size={70}
            color="rgba(255, 255, 255, 0.8)"
          />
          <Text style={styles.noDataText}>There is no any data yet !</Text>
        </View>
      ) : (
        <Checkout navigation={navigation} checkout={checkout} />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
    paddingHorizontal: 7,
  },
  noData: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  noDataText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 1,
    color: '#F4F4F4',
    marginVertical: 10,
  },
  loadingScreen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
