import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Carousel from '../../component/Carousel';
import {Icon} from 'react-native-elements';
import FeaturedField from '../../component/FeaturedField';

export default function HomePage({navigation}) {
  return (
    <ScrollView style={styles.container}>
      <Carousel navigation={navigation} />
      <View style={styles.content}>
        <View style={styles.section}>
          <View style={styles.sectionTitle}>
            <Text style={styles.sectionText}>Featured Field</Text>
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate('FeaturedField')}
            style={styles.sectionAction}>
            <Icon name="arrowright" type="antdesign" size={25} color="#fff" />
          </TouchableOpacity>
        </View>
        <FeaturedField navigation={navigation} />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
    paddingHorizontal: 7,
  },
  header: {
    marginTop: 20,
    marginBottom: 30,
  },
  content: {
    marginVertical: 30,
  },
  section: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 17,
  },
  sectionTitle: {
    paddingLeft: 10,
  },
  sectionAction: {
    paddingRight: 10,
  },
  sectionText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 18,
    letterSpacing: 2,
  },
});
