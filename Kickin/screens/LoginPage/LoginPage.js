import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  StatusBar,
  ToastAndroid,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import LoginForm from '../../component/LoginForm';
import {useDispatch, useSelector} from 'react-redux';
import {
  handleLogin,
  handleResponse,
  googleLogin,
} from '../../redux/Action/AuthAction';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
import Modal from 'react-native-modal';

export default function LoginPage({navigation}) {
  const dispatch = useDispatch();
  const AuthState = useSelector((state) => state.auth);

  const [email, setEmail] = useState('');
  const [wrongEmail, setWrongEmail] = useState(false);
  const [trueEmail, setTrueEmail] = useState(false);
  const [wrongEmailStatement, setWrongEmailStatement] = useState(false);

  const [password, setPassword] = useState('');
  const [wrongPassword, setWrongPassword] = useState(false);
  const [wrongPasswordStatement, setWrongPasswordStatement] = useState(false);
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  const handleEmail = (val) => {
    setEmail(val);
  };

  const handleEmailValidation = () => {
    if (email != '') {
      setWrongEmail(false);
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (regex.test(email)) {
      setWrongEmail(false);
      setTrueEmail(true);
    } else {
      setWrongEmail(true);
      setWrongEmailStatement('Wrong email format');
    }
  };

  const handlePassword = (val) => {
    setPassword(val);
    if (password != '') {
      setWrongPassword(false);
    }
  };

  const updateSecureTextEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const login = async () => {
    if (email == '') {
      setWrongEmail(true);
      setTrueEmail(false);
      setWrongEmailStatement('Email must be filled');
    } else {
      setWrongEmail(false);
      setTrueEmail(true);
      setWrongEmailStatement('');
    }

    if (password == '') {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be filled');
    } else {
      setWrongPassword(false);
      setWrongPasswordStatement('');
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (email != '' && password != '' && regex.test(email)) {
      dispatch(handleLogin(email, password));
    } else {
      ToastAndroid.show('Failed to Log In', ToastAndroid.SHORT);
    }
  };

  const signIn = async () => {
    try {
      GoogleSignin.configure({
        offlineAccess: true,
        webClientId:
          '831344730255-t1n6uemdiehvaja1hvch0e53hgbov09c.apps.googleusercontent.com',
      });
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      const access_token = userInfo.idToken;
      const name = userInfo.user.name;
      dispatch(googleLogin(access_token, name));
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('error occured SIGN_IN_CANCELLED');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('error occured IN_PROGRESS');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('error occured PLAY_SERVICES_NOT_AVAILABLE');
      } else {
        console.log(error);
        console.log('error occured unknow error');
      }
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <StatusBar backgroundColor="transparent" translucent={true} />
      <View style={styles.logo}>
        <Text style={styles.textLogo}>KICKIN</Text>
      </View>
      <LoginForm
        email={email}
        wrongEmail={wrongEmail}
        trueEmail={trueEmail}
        wrongEmailStatement={wrongEmailStatement}
        password={password}
        wrongPassword={wrongPassword}
        wrongPasswordStatement={wrongPasswordStatement}
        secureTextEntry={secureTextEntry}
        handleEmail={handleEmail}
        handlePassword={handlePassword}
        updateSecureTextEntry={updateSecureTextEntry}
        login={login}
        navigation={navigation}
        handleEmailValidation={handleEmailValidation}
        signIn={signIn}
      />
      <Modal isVisible={AuthState.isLoading}>
        <View style={styles.centeredView}>
          <ActivityIndicator size="large" color="#53C9C2" />
        </View>
      </Modal>
    </ScrollView>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#313131',
  },
  logo: {
    marginTop: 30,
    marginBottom: 40,
    alignItems: 'center',
  },
  textLogo: {
    fontFamily: 'Montserrat-BoldItalic',
    fontSize: 30,
    color: '#53C9C2',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 50,
  },
});
