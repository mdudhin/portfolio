import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {Icon} from 'react-native-elements';
import AllFeaturedField from '../../component/AllFeaturedField';
import {useDispatch, useSelector} from 'react-redux';
import {
  getField,
  getFieldPriceDesc,
  getFieldPriceAsc,
  getFieldNameDesc,
  getFieldNameAsc,
  searchFieldByStore,
  getFieldRatingAsc,
  getFieldRatingDesc,
  getFieldFilter,
} from '../../redux/Action/FieldAction';
import Menu, {MenuItem, MenuDivider} from 'react-native-material-menu';

export default function ShowFeaturedField({navigation}) {
  const dispatch = useDispatch();
  const fieldState = useSelector((state) => state.field);

  const [page, setPage] = useState(2);
  const [search, setSearch] = useState(false);
  const [priceDesc, setPriceDesc] = useState(false);
  const [priceAsc, setPriceAsc] = useState(false);
  const [nameDesc, setNameDesc] = useState(false);
  const [nameAsc, setNameAsc] = useState(false);
  const [ratingDesc, setRatingDesc] = useState(false);
  const [ratingAsc, setRatingAsc] = useState(false);
  const [vynil, setVynil] = useState(false);
  const [syntheticGrass, setSyntheticGrass] = useState(false);
  const [parquette, setParquette] = useState(false);
  const [refreshData, setRefreshData] = useState(false);

  const [store, setStore] = useState();

  useEffect(() => {
    dispatch(getField());
  }, [dispatch]);

  const field = fieldState.allField;

  const handleLoadMore = () => {
    const pagePlus = page + 1;
    setPage(pagePlus);
    if (search) {
      dispatch(searchFieldByStore(page, field, store));
    } else if (priceAsc) {
      dispatch(getFieldPriceAsc(page, field));
    } else if (priceDesc) {
      dispatch(getFieldPriceDesc(page, field));
    } else if (nameAsc) {
      dispatch(getFieldNameAsc(page, field));
    } else if (nameDesc) {
      dispatch(getFieldNameDesc(page, field));
    } else if (ratingAsc) {
      dispatch(getFieldRatingAsc(page, field));
    } else if (ratingDesc) {
      dispatch(getFieldRatingDesc(page, field));
    } else if (vynil) {
      dispatch(getFieldFilter(page, field, 'Vynil'));
    } else if (syntheticGrass) {
      dispatch(getFieldFilter(page, field, 'Synthetic Grass'));
    } else if (parquette) {
      dispatch(getFieldFilter(page, field, 'Parquette'));
    } else if (refreshData) {
      refresh();
    } else {
      dispatch(getField(page, field));
    }
  };

  const refresh = () => {
    setRefreshData(true);
    if (search) {
      dispatch(searchFieldByStore(undefined, undefined, store));
    } else if (priceAsc) {
      dispatch(getFieldPriceAsc());
    } else if (priceDesc) {
      dispatch(getFieldPriceDesc());
    } else if (nameAsc) {
      dispatch(getFieldNameAsc());
    } else if (nameDesc) {
      dispatch(getFieldNameDesc());
    } else if (ratingAsc) {
      dispatch(getFieldRatingAsc());
    } else if (ratingDesc) {
      dispatch(getFieldRatingDesc());
    } else if (vynil) {
      dispatch(getFieldFilter(undefined, undefined, 'Vynil'));
    } else if (syntheticGrass) {
      dispatch(getFieldFilter(undefined, undefined, 'Synthetic Grass'));
    } else if (parquette) {
      dispatch(getFieldFilter(undefined, undefined, 'Parquette'));
    } else {
      dispatch(getField(page, field));
    }
  };

  const handleSearch = (val) => {
    setStore(val);
    setSearch(true);
    setPriceAsc(false);
    setPriceDesc(false);
    setNameAsc(false);
    setNameDesc(false);
    setRatingAsc(false);
    setRatingDesc(false);
    setVynil(false);
    setSyntheticGrass(false);
    setParquette(false);
    setRefreshData(false);
    setPage(2);
    dispatch(searchFieldByStore(undefined, undefined, store));
  };

  const menu = useRef();
  const filter = useRef();

  const showMenu = () => menu.current.show();
  const showFilter = () => filter.current.show();

  const searchStore = () => {
    setSearch(true);
    setPriceAsc(false);
    setPriceDesc(false);
    setNameAsc(false);
    setNameDesc(false);
    setRatingAsc(false);
    setRatingDesc(false);
    setVynil(false);
    setSyntheticGrass(false);
    setParquette(false);
    setRefreshData(false);
    setPage(2);
    dispatch(searchFieldByStore(undefined, undefined, store));
  };

  const getPriceDesc = () => {
    menu.current.hide();
    setSearch(false);
    setPriceAsc(false);
    setPriceDesc(true);
    setNameAsc(false);
    setNameDesc(false);
    setRatingAsc(false);
    setRatingDesc(false);
    setVynil(false);
    setSyntheticGrass(false);
    setParquette(false);
    setRefreshData(false);
    setPage(2);
    dispatch(getFieldPriceDesc());
  };

  const getPriceAsc = () => {
    menu.current.hide();
    setSearch(false);
    setPriceAsc(true);
    setPriceDesc(false);
    setNameAsc(false);
    setNameDesc(false);
    setRatingAsc(false);
    setRatingDesc(false);
    setVynil(false);
    setSyntheticGrass(false);
    setParquette(false);
    setRefreshData(false);
    setPage(2);
    dispatch(getFieldPriceAsc());
  };

  const getNameDesc = () => {
    menu.current.hide();
    setSearch(false);
    setPriceAsc(false);
    setPriceDesc(false);
    setNameAsc(false);
    setNameDesc(true);
    setRatingAsc(false);
    setRatingDesc(false);
    setVynil(false);
    setSyntheticGrass(false);
    setParquette(false);
    setRefreshData(false);
    setPage(2);
    dispatch(getFieldNameDesc());
  };

  const getNameAsc = () => {
    menu.current.hide();
    setSearch(false);
    setPriceAsc(false);
    setPriceDesc(false);
    setNameAsc(true);
    setNameDesc(false);
    setRatingAsc(false);
    setRatingDesc(false);
    setVynil(false);
    setSyntheticGrass(false);
    setParquette(false);
    setRefreshData(false);
    setPage(2);
    dispatch(getFieldNameAsc());
  };

  const getRatingDesc = () => {
    menu.current.hide();
    setSearch(false);
    setPriceAsc(false);
    setPriceDesc(false);
    setNameAsc(false);
    setNameDesc(false);
    setRatingAsc(false);
    setRatingDesc(true);
    setVynil(false);
    setSyntheticGrass(false);
    setParquette(false);
    setRefreshData(false);
    setPage(2);
    dispatch(getFieldRatingDesc());
  };

  const getRatingAsc = () => {
    menu.current.hide();
    setSearch(false);
    setPriceAsc(false);
    setPriceDesc(false);
    setNameAsc(false);
    setNameDesc(false);
    setRatingAsc(true);
    setRatingDesc(false);
    setVynil(false);
    setSyntheticGrass(false);
    setParquette(false);
    setRefreshData(false);
    setPage(2);
    dispatch(getFieldRatingAsc());
  };

  const getVynil = () => {
    filter.current.hide();
    setSearch(false);
    setPriceAsc(false);
    setPriceDesc(false);
    setNameAsc(false);
    setNameDesc(false);
    setRatingAsc(false);
    setRatingDesc(false);
    setVynil(true);
    setSyntheticGrass(false);
    setParquette(false);
    setRefreshData(false);
    setPage(2);
    dispatch(getFieldFilter(undefined, undefined, 'Vynil'));
  };

  const getSyntheticGrass = () => {
    filter.current.hide();
    setSearch(false);
    setPriceAsc(false);
    setPriceDesc(false);
    setNameAsc(false);
    setNameDesc(false);
    setRatingAsc(false);
    setRatingDesc(false);
    setVynil(false);
    setSyntheticGrass(true);
    setParquette(false);
    setRefreshData(false);
    setPage(2);
    dispatch(getFieldFilter(undefined, undefined, 'Synthetic Grass'));
  };

  const getParquette = () => {
    filter.current.hide();
    setSearch(false);
    setPriceAsc(false);
    setPriceDesc(false);
    setNameAsc(false);
    setNameDesc(false);
    setRatingAsc(false);
    setRatingDesc(false);
    setVynil(false);
    setSyntheticGrass(false);
    setParquette(true);
    setRefreshData(false);
    setPage(2);
    dispatch(getFieldFilter(undefined, undefined, 'Parquette'));
  };

  return (
    <View style={styles.container}>
      <View style={styles.search}>
        <TextInput
          placeholder="Search"
          placeholderTextColor={'rgba(255, 255, 255, 0.8)'}
          autoCapitalize="none"
          style={styles.textInput}
          onChangeText={(val) => handleSearch(val)}
        />
        <TouchableOpacity onPress={() => searchStore()}>
          <Icon name="search" type="feather" size={25} color="#fff" />
        </TouchableOpacity>
      </View>
      <View style={styles.sortFilter}>
        <Menu
          ref={menu}
          button={
            <View style={styles.sort}>
              <Text style={styles.sortText} onPress={showMenu}>
                Sort
              </Text>
            </View>
          }
          style={styles.menu}>
          <MenuItem onPress={getPriceDesc}>
            <Text style={styles.selectText}>Price Desc</Text>
          </MenuItem>
          <MenuItem onPress={getPriceAsc}>
            <Text style={styles.selectText}>Price Asc</Text>
          </MenuItem>
          <MenuItem onPress={getRatingDesc}>
            <Text style={styles.selectText}>Rating Desc</Text>
          </MenuItem>
          <MenuItem onPress={getRatingAsc}>
            <Text style={styles.selectText}>Rating Asc</Text>
          </MenuItem>
          <MenuItem onPress={getNameDesc}>
            <Text style={styles.selectText}>Name Desc</Text>
          </MenuItem>
          <MenuDivider />
          <MenuItem onPress={getNameAsc}>
            <Text style={styles.selectText}>Name Asc</Text>
          </MenuItem>
        </Menu>
        <Menu
          ref={filter}
          button={
            <View style={styles.sort}>
              <Text style={styles.sortText} onPress={showFilter}>
                Field Type
              </Text>
            </View>
          }
          style={styles.menu}>
          <MenuItem onPress={getVynil}>
            <Text style={styles.selectText}>Vynil</Text>
          </MenuItem>
          <MenuItem onPress={getSyntheticGrass}>
            <Text style={styles.selectText}>Synthetic Grass</Text>
          </MenuItem>
          <MenuItem onPress={getParquette}>
            <Text style={styles.selectText}>Parquette</Text>
          </MenuItem>
        </Menu>
      </View>
      {field.length === 0 ? (
        <View style={styles.noData}>
          <Icon
            name="box-open"
            type="font-awesome-5"
            size={70}
            color="rgba(255, 255, 255, 0.8)"
          />
          <Text style={styles.noDataText}>There is no any data yet !</Text>
        </View>
      ) : (
        <AllFeaturedField
          fieldState={fieldState.allField}
          handleLoadMore={handleLoadMore}
          navigation={navigation}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
    paddingHorizontal: 7,
  },
  search: {
    flexDirection: 'row',
    backgroundColor: '#545454',
    marginTop: 15,
    borderRadius: 5,
    height: 60,
    paddingTop: 16,
    paddingHorizontal: 30,
    marginHorizontal: 25,
  },
  textInput: {
    flex: 1,
    marginTop: -15,
    paddingRight: 15,
    color: '#fff',
    fontFamily: 'Montserrat-Medium',
    fontSize: 16,
    letterSpacing: 1,
  },
  sortFilter: {
    flexDirection: 'row',
  },
  menu: {
    marginHorizontal: 25,
    marginTop: 10,
    backgroundColor: '#545454',
  },
  selectText: {
    color: '#fff',
  },
  sort: {
    backgroundColor: '#545454',
    minWidth: 100,
    paddingVertical: 10,
    marginLeft: 25,
    marginTop: 10,
    borderRadius: 5,
  },
  sortText: {
    marginHorizontal: 18,
    alignSelf: 'center',
    color: '#fff',
    fontFamily: 'Montserrat-Medium',
    fontSize: 16,
    letterSpacing: 1,
  },
  noData: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 80,
  },
  noDataText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 1,
    color: '#F4F4F4',
    marginVertical: 10,
  },
});
