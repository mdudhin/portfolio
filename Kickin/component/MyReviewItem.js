import React, {useState, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import {Icon} from 'react-native-elements';
import Menu, {MenuItem} from 'react-native-material-menu';
import {deleteReview} from '../redux/Action/ReviewAction';
import {useDispatch} from 'react-redux';

export default function MyReviewItem({item, navigation}) {
  const dispatch = useDispatch();

  const [defaultRating, setDefaultRating] = useState();
  const [maxRating, setMaxRating] = useState(5);

  let rating = [];
  for (var i = 1; i <= maxRating; i++) {
    rating.push(
      <TouchableOpacity activeOpacity={0.7} key={i}>
        <Image
          style={styles.StarImage}
          source={
            i <= defaultRating
              ? {
                  uri:
                    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                }
              : {
                  uri:
                    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                }
          }
        />
      </TouchableOpacity>,
    );
  }

  const menu = useRef();

  const showMenu = () => menu.current.show();

  const deleteMyReview = () => {
    dispatch(deleteReview(item.id));
    menu.current.hide();
  };

  const editMyReview = () => {
    menu.current.hide();
    navigation.navigate('EditReview', {
      item: item,
    });
  };

  return (
    <View style={styles.cardView}>
      <View style={styles.header}>
        <TouchableWithoutFeedback
          onPress={() =>
            navigation.navigate('FieldDetail', {
              id: item.field_id,
            })
          }>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.name}>{item.Field.name}</Text>
          </View>
        </TouchableWithoutFeedback>
        <View style={{alignSelf: 'center'}}>
          <Menu
            ref={menu}
            button={
              <View>
                <Icon
                  name="more-vertical"
                  type="feather"
                  size={17}
                  color="#fff"
                  onPress={showMenu}
                />
              </View>
            }
            style={styles.menu}>
            <MenuItem
              onPress={() =>
                navigation.navigate('FieldDetail', {
                  id: item.field_id,
                })
              }>
              <Text style={styles.selectText}>Show Field</Text>
            </MenuItem>
            <MenuItem onPress={() => editMyReview()}>
              <Text style={styles.selectText}>Edit</Text>
            </MenuItem>
            <MenuItem onPress={() => deleteMyReview()}>
              <Text style={styles.selectText}>Delete</Text>
            </MenuItem>
          </Menu>
        </View>
      </View>
      <View style={styles.content}>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.rating}>
            {rating.map((data, i) => (
              <View key={i}>
                <Image
                  style={styles.StarImage}
                  source={
                    i <= item.rating - 1
                      ? {
                          uri:
                            'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                        }
                      : {
                          uri:
                            'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                        }
                  }
                />
              </View>
            ))}
          </View>
        </View>
        <Text style={styles.comment}>{item.comment}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  cardView: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#545454',
    paddingVertical: 15,
    paddingHorizontal: 20,
    marginVertical: 5,
    borderRadius: 5,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  name: {
    color: '#fff',
    fontSize: 13,
    fontFamily: 'Montserrat-Medium',
    alignSelf: 'center',
    marginHorizontal: 2,
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    marginVertical: 10,
  },
  comment: {
    color: '#fff',
    fontSize: 11,
    textAlign: 'justify',
    letterSpacing: 1,
    fontFamily: 'Montserrat-Light',
    marginTop: 7,
  },
  rating: {
    flexDirection: 'row',
    marginVertical: 3,
  },
  StarImage: {
    width: 12,
    height: 12,
    marginHorizontal: 1,
    resizeMode: 'cover',
  },
  date: {
    color: '#fff',
    fontSize: 11,
    letterSpacing: 1,
    fontFamily: 'Montserrat-Light',
    alignSelf: 'center',
    marginHorizontal: 10,
  },
  menu: {
    marginTop: 20,
  },
  selectText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 1,
  },
});
