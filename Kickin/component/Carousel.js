import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Dimensions, Text} from 'react-native';
import CarouselList from 'react-native-snap-carousel';
import Item from './CarouselItem';
import {useDispatch, useSelector} from 'react-redux';
import {getCarousel} from '../redux/Action/FieldAction';
import {Icon} from 'react-native-elements';

export default function Carousel({navigation}) {
  const [activeSlide, setActiveSlide] = useState(0);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCarousel());
  }, [dispatch]);

  const fieldState = useSelector((state) => state.field);

  const field = fieldState.carousel;

  const {width} = Dimensions.get('window');

  return (
    <View style={styles.container}>
      {field.length === 0 ? (
        <View style={styles.noData}>
          <Icon
            name="box-open"
            type="font-awesome-5"
            size={70}
            color="rgba(255, 255, 255, 0.8)"
          />
          <Text style={styles.noDataText}>There is no any data yet !</Text>
        </View>
      ) : (
        <CarouselList
          layout={'default'}
          data={field}
          renderItem={({item}) => {
            return (
              <Item
                item={item}
                length={field.length}
                activeSlide={activeSlide}
                navigation={navigation}
              />
            );
          }}
          onSnapToItem={(index) => setActiveSlide(index)}
          sliderWidth={width - 12}
          itemWidth={width - 75}
          inactiveSlideShift={0.5}
          inactiveSlideOpacity={0.5}
          autoplay={true}
          autoplayDelay={1000}
          autoplayInterval={10000}
          enableMomentum={false}
          lockScrollWhileSnapping={true}
          loop={true}
          slideStyle={{backgroundColor: '#313131'}}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
  },
  noData: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 60,
  },
  noDataText: {
    fontFamily: 'Montserrat-Medium',
    fontSize: 14,
    letterSpacing: 1,
    color: '#F4F4F4',
    marginVertical: 10,
  },
});
