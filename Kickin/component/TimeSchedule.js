import React from 'react';
import {StyleSheet, FlatList, View} from 'react-native';
import Item from './TimeScheduleItem';

export default function TimeSchedule({time, navigation, field}) {
  return (
    <View style={styles.container}>
      <FlatList
        data={time}
        keyExtractor={(item) => `${item.id}`}
        columnWrapperStyle={{justifyContent: 'space-between'}}
        horizontal={false}
        numColumns={2}
        renderItem={({item}) => {
          return <Item item={item} navigation={navigation} field={field} />;
        }}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 45,
    paddingBottom: 15,
  },
});
