import React from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import Item from './FeaturedFieldItem';

export default function AllFeaturedField({
  fieldState,
  handleLoadMore,
  navigation,
}) {
  return (
    <View style={styles.container}>
      <FlatList
        data={fieldState}
        keyExtractor={(item) => `${item.id}`}
        columnWrapperStyle={{justifyContent: 'space-between'}}
        horizontal={false}
        numColumns={2}
        onEndReached={handleLoadMore}
        refreshing={false}
        onRefresh={handleLoadMore}
        onEndReachedThreshold={0.5}
        initialNumToRender={5}
        renderItem={({item}) => {
          return <Item item={item} navigation={navigation} />;
        }}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
  },
});
