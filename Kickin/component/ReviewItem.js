import React, {useState} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

export default function ReviewItem({item}) {
  const [defaultRating, setDefaultRating] = useState(item.rating - 1);
  const [maxRating, setMaxRating] = useState(5);

  const ReviewDate = () => {
    const reviewDate = new Date(item.updatedAt);
    const dates = reviewDate.getDate();
    const month = reviewDate.getMonth();
    const year = reviewDate.getFullYear();
    return <Text style={styles.date}>{`${month + 1}/${dates}/${year}`}</Text>;
  };

  let rating = [];
  for (var i = 1; i <= maxRating; i++) {
    rating.push(
      <TouchableOpacity activeOpacity={0.7} key={i}>
        <Image
          style={styles.StarImage}
          source={
            i <= defaultRating
              ? {
                  uri:
                    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                }
              : {
                  uri:
                    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                }
          }
        />
      </TouchableOpacity>,
    );
  }

  return (
    <View style={styles.cardView}>
      <View style={styles.header}>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.image}
            source={{uri: item.User.Profile.avatar}}
          />
          <Text style={styles.name}>{item.User.Profile.name}</Text>
        </View>
      </View>
      <View style={styles.content}>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.rating}>
            {rating.map((data, i) => (
              <View key={i}>
                <Image
                  style={styles.StarImage}
                  source={
                    i <= defaultRating
                      ? {
                          uri:
                            'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                        }
                      : {
                          uri:
                            'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                        }
                  }
                />
              </View>
            ))}
          </View>
          <ReviewDate />
        </View>
        <Text style={styles.comment}>{item.comment}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  cardView: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#545454',
    paddingVertical: 15,
    paddingHorizontal: 20,
    marginVertical: 5,
    marginHorizontal: 25,
    borderRadius: 5,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  image: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    backgroundColor: '#EAEAEA',
  },
  name: {
    color: '#fff',
    fontSize: 13,
    fontFamily: 'Montserrat-Medium',
    alignSelf: 'center',
    marginHorizontal: 10,
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    marginVertical: 10,
  },
  comment: {
    color: '#fff',
    fontSize: 11,
    textAlign: 'justify',
    letterSpacing: 1,
    fontFamily: 'Montserrat-Light',
    marginTop: 7,
  },
  rating: {
    flexDirection: 'row',
    marginVertical: 3,
  },
  StarImage: {
    width: 12,
    height: 12,
    marginHorizontal: 1,
    resizeMode: 'cover',
  },
  date: {
    color: '#fff',
    fontSize: 11,
    letterSpacing: 1,
    fontFamily: 'Montserrat-Light',
    alignSelf: 'center',
    marginHorizontal: 10,
  },
});
