import React, {useEffect} from 'react';
import {StyleSheet, Text, View, FlatList} from 'react-native';
import Item from './BookHistoryItem';
import {useDispatch} from 'react-redux';
import {getBooking} from '../redux/Action/BookingAction';

export default function BookHistory({navigation, handleLoadMore, booking}) {
  const dispatch = useDispatch();
  return (
    <View style={styles.container}>
      <FlatList
        data={booking}
        keyExtractor={(item) => `${item.id}`}
        onEndReached={handleLoadMore}
        onEndReachedThreshold={0.5}
        initialNumToRender={5}
        refreshing={false}
        onRefresh={() => dispatch(getBooking())}
        renderItem={({item}) => {
          return <Item item={item} navigation={navigation} />;
        }}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
  },
});
