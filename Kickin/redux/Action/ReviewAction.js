import {
  GET_REVIEW,
  ADD_REVIEW,
  GET_MY_REVIEW,
  DELETE_REVIEW,
  EDIT_REVIEW,
} from '../Type/review';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import {ToastAndroid} from 'react-native';
import {getDetailField} from './FieldAction';

export const getReview = (id) => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/review/rating/${id}`,
        {
          headers: {
            Authorization: token,
          },
        },
      );
      if (res !== null) {
        const data = res.data.data;
        dispatch({
          type: GET_REVIEW,
          data: data.overall,
        });
      }
    } catch (e) {
      ToastAndroid.show(e.response.data.errors, ToastAndroid.SHORT);
    }
  };
};

export const getMyReview = (page, review) => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/review/user?page=${
          page === undefined ? 1 : page
        }`,
        {
          headers: {
            Authorization: token,
          },
        },
      );
      if (res !== null) {
        const data = res.data.data.reviews;
        dispatch({
          type: GET_MY_REVIEW,
          data: page === undefined ? data : [...review, ...data],
        });
      }
    } catch (e) {
      ToastAndroid.show(e.response.data.errors, ToastAndroid.SHORT);
    }
  };
};

export const addReview = (id, inputReview, defaultRating) => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      Axios.post(
        `https://be-kickin.herokuapp.com/api/v1/review/create/${id}`,
        {
          comment: inputReview,
          rating: defaultRating + 1,
        },
        {
          headers: {
            Authorization: token,
          },
        },
      )
        .then((res) => {
          if (res.data.status == 'success') {
            const message = 'Success to add review !';
            dispatch({
              type: ADD_REVIEW,
              message: message,
            });
            dispatch(getDetailField(id));
            dispatch(getMyReview());
            ToastAndroid.show(message, ToastAndroid.SHORT);
          }
        })
        .catch((error) => {
          ToastAndroid.show(error.response.data.errors, ToastAndroid.SHORT);
        });
    } catch (e) {
      ToastAndroid.show('failed to make review', ToastAndroid.SHORT);
    }
  };
};

export const editReview = (id, inputReview, defaultRating) => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      Axios.put(
        `https://be-kickin.herokuapp.com/api/v1/review/update/${id}`,
        {
          comment: inputReview,
          rating: defaultRating + 1,
        },
        {
          headers: {
            Authorization: token,
          },
        },
      )
        .then((res) => {
          if (res.data.status == 'success') {
            const message = 'Success to edit review !';
            dispatch({
              type: EDIT_REVIEW,
              message: message,
            });
            dispatch(getMyReview());
            ToastAndroid.show(message, ToastAndroid.SHORT);
          }
        })
        .catch((error) => {
          ToastAndroid.show(error.response.data.errors, ToastAndroid.SHORT);
        });
    } catch (e) {
      ToastAndroid.show('failed to edit review', ToastAndroid.SHORT);
    }
  };
};

export const deleteReview = (id) => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      Axios.delete(
        `https://be-kickin.herokuapp.com/api/v1/review/delete/${id}`,
        {
          headers: {
            Authorization: token,
          },
        },
      )
        .then((res) => {
          if (res.data.status == 'success') {
            const message = 'Success to delete review !';
            dispatch({
              type: DELETE_REVIEW,
              message: message,
            });
            dispatch(getMyReview());
            ToastAndroid.show(message, ToastAndroid.SHORT);
          }
        })
        .catch((error) => {
          ToastAndroid.show(error.response.data.errors, ToastAndroid.SHORT);
        });
    } catch (e) {
      ToastAndroid.show('failed to make review', ToastAndroid.SHORT);
    }
  };
};
