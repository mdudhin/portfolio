import Axios from 'axios';
import {
  GET_CHECKOUT,
  GET_BOOKING,
  CREATE_BOOKING,
  PAY_BOOKING,
  SHOW_LOADING,
} from '../Type/booking';
import AsyncStorage from '@react-native-community/async-storage';
import {ToastAndroid} from 'react-native';

export const getCheckout = () => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = await Axios.get(
        'https://be-kickin.herokuapp.com/api/v1/booking/checkoutrn',
        {
          headers: {
            Authorization: token,
          },
        },
      );
      if (res !== null) {
        const data = res.data.data.bookings;
        dispatch({
          type: GET_CHECKOUT,
          data: data,
        });
      }
    } catch (e) {
      ToastAndroid.show(e.response.data.errors, ToastAndroid.SHORT);
    }
  };
};

export const getBooking = (page, booking) => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = await Axios.get(
        `https://be-kickin.herokuapp.com/api/v1/booking/list?page=${
          page === undefined ? 1 : page
        }`,
        {
          headers: {
            Authorization: token,
          },
        },
      );
      if (res !== null) {
        const data = res.data.data.showBookingList;
        dispatch({
          type: GET_BOOKING,
          data: page === undefined ? data : [...booking, ...data],
        });
      }
    } catch (e) {
      console.log(e.response.data.errors);
      ToastAndroid.show(e.response.data.errors, ToastAndroid.SHORT);
    }
  };
};

export const createBooking = (id, date, schedule) => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      Axios.put(
        `https://be-kickin.herokuapp.com/api/v1/booking/create/${id}`,
        {
          date: date,
          schedule_time: schedule,
        },
        {
          headers: {
            Authorization: token,
          },
        },
      )
        .then((res) => {
          if (res.data.status == 'success') {
            const message = 'Success to Book !';
            dispatch({
              type: CREATE_BOOKING,
              message: message,
            });
            dispatch(getCheckout());
            ToastAndroid.show(message, ToastAndroid.SHORT);
          }
        })
        .catch((error) => {
          ToastAndroid.show(error.response.data.errors, ToastAndroid.SHORT);
        });
    } catch (e) {
      ToastAndroid.show('failed to book', ToastAndroid.SHORT);
    }
  };
};

export const payBooking = (id, imagePayment, imageName) => {
  return async (dispatch) => {
    dispatch({type: SHOW_LOADING, isLoading: true});
    try {
      const token = await AsyncStorage.getItem('@token');
      const data = new FormData();
      data.append('image', {
        uri: imagePayment,
        name: imageName,
        type: 'image/jpg',
      });
      Axios.post(
        `https://be-kickin.herokuapp.com/api/v1/booking/pay/${id}`,
        data,
        {
          headers: {
            Authorization: token,
            'Content-Type': 'application/json',
          },
        },
      )
        .then((res) => {
          if (res.data.status == 'success') {
            const message = 'Success to send payment Slip !';
            dispatch({
              type: PAY_BOOKING,
            });
            dispatch(getCheckout());
            ToastAndroid.show(message, ToastAndroid.SHORT);
          }
        })
        .catch((error) => {
          dispatch({type: SHOW_LOADING, isLoading: false});
          ToastAndroid.show(error.response.data.errors, ToastAndroid.SHORT);
        });
    } catch (e) {
      dispatch({type: SHOW_LOADING, isLoading: false});
      ToastAndroid.show(e, ToastAndroid.SHORT);
    }
  };
};
