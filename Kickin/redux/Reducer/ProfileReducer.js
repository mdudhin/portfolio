import {
  GET_USER,
  FAILED_GET_USER,
  EDIT_USER,
  FAILED_EDIT_USER,
  SHOW_LOADING_PROFILE,
  CLEAR_RESPONSE,
} from '../Type/profile';

const initialState = {
  user: [],
  message: null,
  isResponse: false,
  isLoading: false,
};

export const ProfileReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER:
      return {
        ...state,
        user: action.user,
      };
    case FAILED_GET_USER:
      return {
        ...state,
        message: action.message,
      };
    case EDIT_USER:
      return {
        ...state,
        message: action.message,
        isLoading: false,
      };
    case FAILED_EDIT_USER:
      return {
        ...state,
        message: action.message,
        isLoading: false,
      };
    case SHOW_LOADING_PROFILE:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case CLEAR_RESPONSE:
      return {
        ...state,
        message: null,
      };
    default:
      return state;
  }
};
