import {
  GET_FIELD,
  FAILED_GET_FIELD,
  GET_DETAIL_FIELD,
  FAILED_GET_DETAIL_FIELD,
  GET_CAROUSEL,
} from '../Type/field';

const initialState = {
  carousel: [],
  allField: [],
  detailField: [],
  store: [],
  review: [],
  message: null,
};

export const FieldReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CAROUSEL:
      return {
        ...state,
        carousel: action.data,
      };
    case GET_FIELD:
      return {
        ...state,
        allField: action.data,
      };
    case FAILED_GET_FIELD:
      return {
        ...state,
        message: action.message,
      };
    case GET_DETAIL_FIELD:
      return {
        ...state,
        detailField: action.data,
        store: action.store,
        review: action.review,
      };
    case FAILED_GET_DETAIL_FIELD:
      return {
        ...state,
        message: action.message,
      };
    default:
      return state;
  }
};
