import React, {useState, useEffect} from 'react';
import {StyleSheet, View, ActivityIndicator} from 'react-native';
import {Icon} from 'react-native-elements';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import LoginScreen from './screens/Login/Login';
import RegisterScreen from './screens/Register/Register';
import HomeScreen from './screens/Home/Home';
import ImportanceScreen from './screens/Importance/Importance';
import CompletionScreen from './screens/Completion/Completion';
import ProfileScreen from './screens/Profile/Profile';
import {AuthContext} from './screens/Context';
// import AsyncStorage from '@react-native-community/async-storage';
import * as Keychain from 'react-native-keychain';
import {TouchableOpacity} from 'react-native-gesture-handler';
import DrawerContent from './screens/DrawerContent';
import SplashScreen from './screens/Splash';

const App = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [userToken, setUserToken] = useState(null);

  const authContext = React.useMemo(() => {
    return {
      signIn: async (token) => {
        setIsLoading(false);
        setUserToken(token);
      },
      signUp: async () => {},
      signOut: () => {
        setIsLoading(false);
        setUserToken(null);
      },
    };
  }, []);

  const Loading = () => {
    return (
      <View style={styles.loadingScreen}>
        <ActivityIndicator size="large" />
      </View>
    );
  };

  const getData = async () => {
    try {
      // const token = await AsyncStorage.getItem('@token');
      // setUserToken(token);
      const credentials = await Keychain.getGenericPassword();
      console.log(credentials.username);
      setUserToken(credentials.password);
    } catch (e) {
      console.log('No token');
    }
  };

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
      getData();
    }, 2000);
  }, []);

  if (isLoading) {
    return <SplashScreen />;
  }

  const AuthStack = createStackNavigator();
  const AuthStackScreen = () => {
    return (
      <AuthStack.Navigator>
        <AuthStack.Screen
          name="Login"
          component={LoginScreen}
          options={{headerShown: false}}
        />
        <AuthStack.Screen
          name="Register"
          component={RegisterScreen}
          options={{headerShown: false}}
        />
      </AuthStack.Navigator>
    );
  };

  const HomeStack = createStackNavigator();
  const HomeStackScreen = ({navigation}) => {
    return (
      <HomeStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#2f80ed',
            elevation: 0,
            shadowOpacity: 0,
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
        }}>
        <HomeStack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: 'Home',
            headerLeft: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.openDrawer();
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="menu" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
      </HomeStack.Navigator>
    );
  };

  const ImportanceStack = createStackNavigator();
  const ImportanceStackScreen = ({navigation}) => {
    return (
      <ImportanceStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#2f80ed',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
        }}>
        <ImportanceStack.Screen
          name="Importance"
          component={ImportanceScreen}
          options={{
            title: 'Importance',
            headerLeft: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.openDrawer();
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="menu" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
      </ImportanceStack.Navigator>
    );
  };

  const CompletionStack = createStackNavigator();
  const CompletionStackScreen = ({navigation}) => {
    return (
      <CompletionStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#2f80ed',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
        }}>
        <CompletionStack.Screen
          name="Completion"
          component={CompletionScreen}
          options={{
            title: 'Completion',
            headerLeft: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.openDrawer();
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="menu" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
      </CompletionStack.Navigator>
    );
  };

  const ProfileStack = createStackNavigator();
  const ProfileStackScreen = ({navigation}) => {
    return (
      <ProfileStack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: '#2f80ed',
          },
          headerTintColor: '#fff',
          headerTitleAlign: 'center',
        }}>
        <ProfileStack.Screen
          name="Completion"
          component={ProfileScreen}
          options={{
            title: 'Profile',
            headerLeft: () => (
              <TouchableOpacity
                onPress={() => {
                  navigation.openDrawer();
                }}
                style={{paddingHorizontal: 13}}>
                <Icon name="menu" type="feather" size={23} color="#fff" />
              </TouchableOpacity>
            ),
          }}
        />
      </ProfileStack.Navigator>
    );
  };

  const Drawer = createDrawerNavigator();
  const DrawerScreen = () => {
    return (
      <Drawer.Navigator drawerContent={(props) => <DrawerContent {...props} />}>
        <Drawer.Screen
          name="Home"
          component={HomeStackScreen}
          options={{
            drawerIcon: () => (
              <Icon name="home" type="feather" size={23} color="#2d3436" />
            ),
          }}
        />
        <Drawer.Screen
          name="Importance"
          component={ImportanceStackScreen}
          options={{
            drawerIcon: () => (
              <Icon name="star" type="feather" size={23} color="#2d3436" />
            ),
          }}
        />
        <Drawer.Screen
          name="Completion"
          component={CompletionStackScreen}
          options={{
            drawerIcon: () => (
              <Icon
                name="check-square"
                type="feather"
                size={23}
                color="#2d3436"
              />
            ),
          }}
        />
        <Drawer.Screen
          name="Profile"
          component={ProfileStackScreen}
          options={{
            drawerIcon: () => (
              <Icon name="user" type="feather" size={23} color="#2d3436" />
            ),
          }}
        />
      </Drawer.Navigator>
    );
  };

  const RootStack = createStackNavigator();

  return (
    <AuthContext.Provider value={authContext}>
      <NavigationContainer>
        <RootStack.Navigator>
          {!userToken ? (
            <RootStack.Screen
              name="Auth"
              component={AuthStackScreen}
              options={{headerShown: false}}
            />
          ) : (
            <RootStack.Screen
              name="App"
              component={DrawerScreen}
              options={{headerShown: false}}
            />
          )}
        </RootStack.Navigator>
      </NavigationContainer>
    </AuthContext.Provider>
  );
};

export default App;

const styles = StyleSheet.create({
  loadingScreen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
