import React from 'react';
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Icon} from 'react-native-elements';

const ProfileForm = ({
  userId,
  name,
  photoProfile,
  wrongName,
  handleName,
  updateProfile,
  isLoading,
}) => {
  return (
    <View>
      <View style={styles.action}>
        <Text style={styles.actionLabel}>Name</Text>
        <View style={styles.actionBody}>
          <TextInput
            placeholder="Enter Name"
            autoCapitalize="none"
            defaultValue={name}
            style={styles.textInput}
            onChangeText={(val) => handleName(val)}
          />
          {wrongName ? (
            <Icon
              name="alert-circle"
              type="feather"
              size={20}
              color="#d63031"
            />
          ) : (
            <Icon
              name="check-circle"
              type="feather"
              size={20}
              color="transparent"
            />
          )}
        </View>
      </View>
      {wrongName ? (
        <Text style={styles.wrongStatement}>The form must be filled</Text>
      ) : (
        <Text />
      )}
      <View style={styles.actionButton}>
        <TouchableOpacity
          onPress={() => updateProfile(userId, name, photoProfile)}
          style={styles.btn}>
          {isLoading ? (
            <ActivityIndicator size="small" color="#fff" />
          ) : (
            <Text style={styles.btnText}>Edit</Text>
          )}
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ProfileForm;

const styles = StyleSheet.create({
  action: {
    flexDirection: 'column',
    marginTop: 15,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: 'grey',
    paddingTop: 15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  actionLabel: {
    marginTop: -5,
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  actionBody: {
    flexDirection: 'row',
    borderColor: 'grey',
    paddingTop: 7,
  },
  textInput: {
    flex: 1,
    marginTop: -12,
    paddingLeft: 12,
    color: '#05375a',
  },
  wrongStatement: {
    paddingLeft: 20,
    color: '#d63031',
  },
  actionButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: 15,
  },
  btn: {
    backgroundColor: '#2F80ED',
    paddingVertical: 10,
    paddingHorizontal: 40,
    borderRadius: 30,
  },
  btnText: {
    color: '#fff',
  },
  signIn: {
    flexDirection: 'row',
    alignSelf: 'center',
    paddingTop: 15,
  },
});
