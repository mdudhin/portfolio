import React, {Component} from 'react';
import {Text, StyleSheet, View, Button, TextInput} from 'react-native';
import {connect} from 'react-redux';
import {change_name} from '../../src/redux/Action/index';

class Home extends Component {
  render() {
    return (
      <View>
        <Text> Name : {this.props.name} </Text>
        <TextInput
          onChangeText={(name) => this.props.change_name(name)}
          style={{borderWidth: 1}}
        />
        <Button
          title="go detail"
          onPress={() => this.props.navigation.navigate('Detail')}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  name: state.auth.name,
});

export default connect(mapStateToProps, {change_name})(Home);
