import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import {Icon} from 'react-native-elements';
import Modal from 'react-native-modal';
import FormEditTodo from './FormEditTodo';

const Item = ({
  id,
  title,
  description,
  dueDate,
  completion,
  importance,
  todoComplete,
  todoImportance,
  todoDelete,
  todoEdit,
}) => {
  const [formatDueDate, setFormatDueDate] = useState(null);
  const [isModalVisible, setModalVisible] = useState(false);
  const [fdueDate, setFDueDate] = useState('');

  useEffect(() => {
    formatDate();
  });

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
    const currentDate = new Date(dueDate);
    const dates = currentDate.getDate();
    const month = currentDate.getMonth();
    const year = currentDate.getFullYear();
    setFDueDate(`${month + 1}/${dates}/${year}`);
  };

  const editTodo = (id, title, description, dueDate) => {
    setModalVisible(!isModalVisible);
    todoEdit(id, title, description, dueDate);
  };

  const deleteAlert = (id) =>
    Alert.alert(
      'Are you sure ?',
      "You won't be able to revert this!",
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => todoDelete(id)},
      ],
      {cancelable: false},
    );

  const formatDate = () => {
    const currentDate = new Date(dueDate);
    const day = currentDate.getDay();
    const date = currentDate.getDate();
    const month = currentDate.getMonth();
    const year = currentDate.getFullYear();
    const dList = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ];
    const mList = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    setFormatDueDate(`${dList[day]}, ${date} ${mList[month]} ${year}`);
  };

  return (
    <View style={styles.item}>
      <TouchableOpacity
        style={styles.complete}
        onPress={() => todoComplete(id)}>
        {completion ? <Icon name="check" type="feather" size={25} /> : <View />}
      </TouchableOpacity>
      <TouchableWithoutFeedback onPress={toggleModal}>
        <View style={styles.content}>
          <Text style={styles.contentTitle}>{title}</Text>
          <Text style={styles.contentDueDate}>Due date : {formatDueDate}</Text>
        </View>
      </TouchableWithoutFeedback>
      <View style={styles.action}>
        <TouchableOpacity
          style={styles.importance}
          onPress={() => todoImportance(id)}>
          {importance ? (
            <Icon name="star" type="fontawesome" size={30} color="#f39c12" />
          ) : (
            <Icon name="star" type="feather" size={30} color="#f39c12" />
          )}
        </TouchableOpacity>
        <View style={styles.actionRow}>
          <TouchableOpacity style={styles.edit}>
            <Icon name="edit" type="feather" size={20} onPress={toggleModal} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.delete}>
            <Icon
              name="trash"
              type="feather"
              size={20}
              onPress={() => deleteAlert(id)}
            />
          </TouchableOpacity>
        </View>
      </View>
      <Modal isVisible={isModalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.addModal}>
            <TouchableWithoutFeedback onPress={toggleModal}>
              <View style={styles.closeBtnModal}>
                <Icon name="x" type="feather" size={30} />
              </View>
            </TouchableWithoutFeedback>
            <FormEditTodo
              dataId={id}
              dataTitle={title}
              dataDescription={description}
              dataDueDate={dueDate}
              fdueDate={fdueDate}
              editTodo={editTodo}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default Item;

const styles = StyleSheet.create({
  item: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#2f80ed',
    marginVertical: 13,
    marginHorizontal: 16,
    borderRadius: 20,
  },
  complete: {
    backgroundColor: '#0055ba',
    justifyContent: 'center',
    width: '15%',
    height: 60,
    borderRadius: 20,
  },
  content: {
    width: '60%',
    paddingVertical: 5,
    paddingHorizontal: 15,
    justifyContent: 'center',
  },
  contentTitle: {
    fontSize: 18,
  },
  contentDueDate: {
    fontSize: 11,
  },
  action: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#0055ba',
    height: 60,
    borderRadius: 20,
  },
  importance: {
    paddingHorizontal: 8,
    borderRightWidth: 1,
    justifyContent: 'center',
  },
  actionRow: {
    justifyContent: 'center',
  },
  edit: {
    paddingHorizontal: 8,
    paddingVertical: 3,
    justifyContent: 'center',
    borderBottomWidth: 1,
  },
  delete: {
    paddingHorizontal: 5,
    paddingVertical: 3,
    justifyContent: 'center',
  },
  closeBtn: {
    backgroundColor: '#0055ba',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 50,
  },
  addModal: {
    backgroundColor: '#fff',
    margin: 20,
    borderRadius: 20,
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  closeBtnModal: {
    alignSelf: 'flex-end',
    width: 40,
    height: 40,
    marginTop: -20,
  },
});
