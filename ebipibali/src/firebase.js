import * as firebase from 'firebase';
require('firebase/firestore');

var firebaseConfig = {
	apiKey: 'AIzaSyBIARsCiYgVsiRAcRMIdtgENMTDnBCn2eY',
	authDomain: 'ebipibali-17c7c.firebaseapp.com',
	databaseURL: 'https://ebipibali-17c7c.firebaseio.com',
	projectId: 'ebipibali-17c7c',
	storageBucket: 'ebipibali-17c7c.appspot.com',
	messagingSenderId: '908921731989',
	appId: '1:908921731989:web:57abde0282fc9ca3a4149c',
	measurementId: 'G-XBGSHCNYE5',
};
// Initialize Firebase
const db = firebase.initializeApp(firebaseConfig);
const dbFire = firebase.firestore();
export { db, dbFire };
firebase.analytics();
