import Vue from 'vue';
import Vuex from 'vuex';
import { db, dbFire } from '@/firebase';

Vue.use(Vuex);

dbFire
	.collection('packagePost')
	.get()
	.then(querySnapshot => {
		if (querySnapshot.empty) {
			//this.$router.push('/HelloWorld')
		} else {
			var packagesArray = [];
			querySnapshot.forEach(doc => {
				let packages = doc.data();
				packages.id = doc.id;
				packagesArray.push(packages);
			});
			store.commit('setPackages', packagesArray);
		}
	});

dbFire
	.collection('recTourPost')
	.get()
	.then(querySnapshot => {
		if (querySnapshot.empty) {
			//this.$router.push('/HelloWorld')
		} else {
			var recTourArray = [];
			querySnapshot.forEach(doc => {
				let recTour = doc.data();
				recTour.id = doc.id;
				recTourArray.push(recTour);
			});
			store.commit('setRecTour', recTourArray);
		}
	});

dbFire
	.collection('packageTour')
	.get()
	.then(querySnapshot => {
		if (querySnapshot.empty) {
			//this.$router.push('/HelloWorld')
		} else {
			var packageTourArray = [];
			querySnapshot.forEach(doc => {
				let packageTour = doc.data();
				packageTour.id = doc.id;
				packageTourArray.push(packageTour);
			});
			store.commit('setPackageTour', packageTourArray);
		}
	});

const store = new Vuex.Store({
	state: {
		user: null,
		drawer: false,
		packages: [],
		recTour: [],
		packageTour: [],
	},
	getters: {
		detailPackages(state) {
			return packageId => {
				return state.packages.find(item => {
					return item.id === packageId;
				});
			};
		},
		detailRecTour(state) {
			return recTourId => {
				return state.recTour.find(item => {
					return item.id === recTourId;
				});
			};
		},
		detailPackageTour(state) {
			return packageTourId => {
				return state.packageTour.find(item => {
					return item.id === packageTourId;
				});
			};
		},
		user(state) {
			return state.user;
		},
	},
	mutations: {
		setUser(state, payload) {
			state.user = payload;
		},
		setPackages(state, val) {
			if (val) {
				state.packages = val;
			} else {
				state.packages = [];
			}
		},
		setRecTour(state, val) {
			if (val) {
				state.recTour = val;
			} else {
				state.recTour = [];
			}
		},
		setPackageTour(state, val) {
			if (val) {
				state.packageTour = val;
			} else {
				state.packageTour = [];
			}
		},
	},
	actions: {
		login({ commit }, payload) {
			db.auth()
				.signInWithEmailAndPassword(payload.email, payload.password)
				.then(user => {
					const newUser = {
						id: user.uid,
					};
					commit('setUser', newUser);
				});
		},
	},
});

export default store;
