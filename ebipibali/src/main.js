import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import VueResizeText from 'vue-resize-text';
import Vuelidate from 'vuelidate';
import { db } from './firebase';
import VueFirestore from 'vue-firestore';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import CKEditor from '@ckeditor/ckeditor5-vue';
import VueTheMask from 'vue-the-mask';

Vue.use(VueTheMask);
Vue.use(CKEditor);
Vue.use(VueSweetalert2);
Vue.use(db);
Vue.use(VueFirestore);
Vue.use(Vuelidate);
Vue.config.productionTip = false;
Vue.use(VueResizeText);

router.beforeEach((to, from, next) => {
	const requiresAuth = to.matched.some(x => x.meta.requiresAuth);
	const requiresGuest = to.matched.some(x => x.meta.requiresGuest);
	const currentUser = db.auth().currentUser;

	if (requiresAuth && !currentUser) {
		next('/login');
	} else if (requiresGuest && currentUser) {
		next('/homePost');
	} else {
		next();
	}
});

let app;

db.auth().onAuthStateChanged(user => {
	if (!app) {
		app = new Vue({
			router,
			store,
			vuetify,
			user,
			render: h => h(App),
		}).$mount('#app');
	}
});
