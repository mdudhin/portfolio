import Vue from 'vue';
import Router from 'vue-router';
import App from '../views/App.vue';
import Home from '../views/Home.vue';
import About from '../views/About.vue';
import contactUs from '../views/Contact.vue';
import Package from '../views/Package.vue';
import DetailPackage from '../components/DetailPackage.vue';
import Admin from '../views/Admin/Home.vue';
import Dashboard from '../views/Admin/Dashboard.vue';
import HomePost from '../views/Admin/HomePost.vue';
import AboutPost from '../views/Admin/AboutPost.vue';
import PackagePost from '../views/Admin/PackagePost.vue';
import Messages from '../views/Admin/Messages.vue';
import Reservation from '../views/Admin/Reservation.vue';
import Login from '../components/Auth/Login.vue';
import ContactPost from '../views/Admin/ContactPost.vue';
import rsvComplete from '../views/Admin/rsvComplete.vue';
import RecTour from '../views/RecTour';
import RecTourPost from '../views/Admin/RecTourPost.vue';
import DetailRecTour from '../components/DetailRecTour.vue';
import PackageTour from '../views/PackageTour.vue';
import DetailPackTour from '../components/DetailPackageTour.vue';
import PackageTourPost from '../views/Admin/PackageTourPost.vue';

Vue.use(Router);

export default new Router({
	mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			name: 'app',
			component: App,
			children: [
				{
					path: '',
					name: 'home',
					component: Home,
				},
				{
					path: 'about',
					name: 'about',
					component: About,
				},
				{
					path: 'contactus',
					name: 'contactus',
					component: contactUs,
				},
				{
					path: 'package',
					name: 'package',
					component: Package,
				},
				{
					path: 'package/:id',
					name: 'DetailPackage',
					props: true,
					component: DetailPackage,
				},
				{
					path: 'recTour',
					name: 'RecommendedTour',
					props: true,
					component: RecTour,
				},
				{
					path: 'recTour/:id',
					name: 'DetailRecTour',
					props: true,
					component: DetailRecTour,
				},
				{
					path: 'packageTour',
					name: 'PackageTour',
					props: true,
					component: PackageTour,
				},
				{
					path: 'packageTour/:id',
					name: 'DetailPackTour',
					props: true,
					component: DetailPackTour,
				},
			],
		},
		{
			path: '/login',
			name: 'login',
			component: Login,
			meta: { requiresGuest: true },
		},
		{
			path: '/dashboard',
			name: 'admin',
			component: Admin,
			meta: { requiresAuth: true },
			children: [
				{
					path: '/dashboard',
					name: 'Dashboard',
					component: Dashboard,
				},
				{
					path: '/homePost',
					name: 'HomePost',
					component: HomePost,
				},
				{
					path: '/aboutPost',
					name: 'AboutPost',
					component: AboutPost,
				},
				{
					path: '/contactPost',
					name: 'ContactPost',
					component: ContactPost,
				},
				{
					path: '/packagePost',
					name: 'PackagePost',
					component: PackagePost,
				},
				{
					path: '/recTourPost',
					name: 'RecTourPost',
					component: RecTourPost,
				},
				{
					path: '/packageTourPost',
					name: 'PackageTourPost',
					component: PackageTourPost,
				},
				{
					path: '/messages',
					name: 'Messages',
					component: Messages,
				},
				{
					path: '/reservation',
					name: 'Reservation',
					component: Reservation,
				},
				{
					path: '/rsvComplete',
					name: 'Complete Reservation',
					component: rsvComplete,
				},
			],
		},
	],
	scrollBehavior(to, from, savedPosition) {
		if (savedPosition) {
			return savedPosition;
		} else if (savedPosition === null) {
			return { x: 0, y: 0 };
		}
	},
});
