# Portfolio

Welcome to my portfolio, in this portfolio you can see all of my progress in learning programming

list project

# Calculator

This is a first project when i learn javascript, i make a simple calculator using React JS Library

How To Install :

- yarn Install
- yarn start

The app look like :

<div align="center">
    <img src="/uploads/943d3ebec29b8e9c8494d6c3d1f166fb/Screenshot_from_2020-08-19_12-24-37.png" width="600px"</img> 
</div>

# Todo App

Todo App is my first app with react native, i make this app with my team at glints academy and my role is Mobile Developer

Library i used :
- react-navigation
- @react-native-community/datetimepicker
- react-native-elements
- react-native-image-picker
- react-native-keychain
- react-native-modal
- react-native-vector-icons
- axios
- moment

How To Install :

- yarn Install
- react-native start
- react-native run-android

The app look like :

<div align="center">
    <img src="/uploads/3d4eae615fb0e99acfcf0f2228540eee/screenshot-2020-08-19_12.43.42.004.png" width="200px"</img>
    <img src="/uploads/333548074d7313c836e6d1ba8be70c5f/screenshot-2020-08-19_12.43.50.723.png" width="200px"</img>
    <img src="/uploads/5b4ee4f398ebdcf8c21819b0c68567b3/screenshot-2020-08-19_12.44.03.055.png" width="200px"</img> 
</div>

# Movie Review App

Movie Review App is an app that allowed user to make a review for movie or add a watchlist, i make this app with my team at glints academy and my role is Mobile Developer

Library i used :
- react-navigation
- @react-native-community/datetimepicker
- @react-native-community/async-storage
- react-native-elements
- react-native-image-picker
- react-native-anchor-carousel
- react-native-modal
- react-native-vector-icons
- axios
- redux
- react-redux
- redux-thunk
- react-native-animatable
- react-native-webview

How To Install :

- yarn Install
- react-native start
- react-native run-android

The app look like :

<div align="center">
    <img src="/uploads/184dd557cef7fde03548629de883a0e7/screenshot-2020-08-19_12.57.43.685.png" width="200px"</img>
    <img src="/uploads/16fea7174324e777b87f056245d357f7/screenshot-2020-08-19_12.58.03.6.png" width="200px"</img>
    <img src="/uploads/753d3c349ee93151ef547aaa76c21921/screenshot-2020-08-19_12.59.11.815.png" width="200px"</img>
    <img src="/uploads/7eca4f30952f1a4f248156327b0169ed/screenshot-2020-08-19_12.58.37.424.png" width="200px"</img>
</div>

# Kickin App

Kickin is a platform for futsal court bookings, i make this app with my team at glints academy and my role is Mobile Developer

Library i used :
- react-navigation
- @react-native-community/async-storage
- @react-native-community/google-signin
- axios
- moment
- react-native-elements
- react-native-image-picker
- react-native-linear-gradient
- react-native-material-menu
- react-native-modal
- react-native-push-notification
- react-native-snap-carousel
- react-native-vector-icons
- redux
- react-redux
- redux-persist
- redux-thunk

How To Install :

- yarn Install
- react-native start
- react-native run-android

The app look like :

<div align="center">
    <img src="/uploads/f664dfa73a7ee924237a831f2a88490b/screenshot-2020-08-19_13.13.29.156.png" width="200px"</img>
    <img src="/uploads/145de6b0c8f965b5eb8b924bb7bdee8e/screenshot-2020-08-19_13.13.58.717.png" width="200px"</img>
    <img src="/uploads/9bc482ea9d4a0a6476c099eeb6e91c8b/screenshot-2020-08-19_13.15.28.049.png" width="200px"</img>
    <img src="/uploads/ee66bb2c70884096364a52634ba20f44/screenshot-2020-08-19_13.15.12.731.png" width="200px"</img>
    <img src="/uploads/38fb6ae90138d58b0668b2b3e9a25d64/Schedule_Page.png" width="200px"</img>
    <img src="/uploads/87c9696c2b842d67c9037c95c9bd0d5a/Payment_Page_2.2.png" width="200px"</img>
    <img src="/uploads/572426fd6d970d88384ccd67249c02a6/Notification.png" width="200px"</img>
    <img src="/uploads/ce052c737db593d04aec6be8b4a2a800/Ticket_Page_1.png" width="200px"</img>
</div>

# Web Ebipibali

Ebipibali is a website for tour guide and tour package reservation. in this app i use Vue JS

Library i used :
- Firebase
- vue-firestore
- vue-resize-text
- vue-router
- vue-sweetalert2
- vuelidate
- ckeditor

How To Install :

- yarn Install
- yarn serve

The app look like :

<div align="center">
    <img src="/uploads/6cb1ac6af25b0dbcb930249cdac1047d/Screenshot_1.png" width="600px"</img>
    <img src="/uploads/762ce8aba577183d555aab945c9ce2ee/Screenshot_2.png" width="600px"</img>
    <img src="/uploads/c4db57553dd9a5e0ca968fcf9279b640/Screenshot_4.png" width="600px"</img>
    <img src="/uploads/f999c2e29334e210f1b25a54e274092e/Screenshot_5.png" width="600px"</img>
</div>