import React, { Component } from 'react'
import ResultComponent from './component/ResultComponent.js';
import KeyPadComponent from './component/KeyPadComponent.js';
import './App.css'

export default class App extends Component {
  constructor(){
    super();

    this.state = {
      previousOperand: '',
      currentOperand: '',
      operation: undefined
    };

    this.default()
  }

  default() {
		this.currentOperand = '';
		this.previousOperand = '';
		this.operation = undefined;
	}

  number = button => {
    this.appendNumber(button);
    this.updateDisplay()
  };

  operationBtn = button => {
    this.chooseOperation(button);
    this.updateDisplay();
  };

  equals = () => {
    this.compute();
    this.updateDisplay();
  };

  clear = () => {
    this.default();
    this.updateDisplay();
  }

  delete = () => {
    this.currentOperand = this.currentOperand.toString().slice(0, -1);
    this.updateDisplay();
  }

  appendNumber(number) {
		if (number === '.' && this.currentOperand.includes('.')) return;
    this.currentOperand = this.currentOperand.toString() + number.toString();
	}

  chooseOperation = (operation) => {
    if (this.currentOperand === '') return;
    if (this.previousOperand !== '') {
      this.compute();
    }
    this.operation = operation;
    this.previousOperand = this.currentOperand;
    this.currentOperand = '';
  }

  compute = () => {
    let computation;
    const prev = parseFloat(this.previousOperand);
    const current = parseFloat(this.currentOperand);
    if (isNaN(prev) || isNaN(current)) return;
    switch (this.operation) {
      case '+':
        computation = prev + current;
        break;
      case '-':
        computation = prev - current;
        break;
      case '*':
        computation = prev * current;
        break;
      case '÷':
        computation = prev / current;
        break;
      case 'MAX':
        computation = Math.max(prev, current);
        break;
      case 'MIN':
        computation = Math.min(prev, current);
        break;
      default:
        return;
    }
    this.currentOperand = computation;
    this.operation = undefined;
    this.previousOperand = '';
  }

  getDisplayNumber(number) {
		const stringNumber = number.toString();
		const integerDigits = parseFloat(stringNumber.split('.')[0]);
		const decimalDigits = stringNumber.split('.')[1];
		let integerDisplay;
		if (isNaN(integerDigits)) {
			integerDisplay = '';
		} else {
			integerDisplay = integerDigits.toLocaleString('en', {
				maximumFractionDigits: 0,
			});
		}
		if (decimalDigits != null) {
			return `${integerDisplay}.${decimalDigits}`;
		} else {
			return integerDisplay;
		}
	}

  updateDisplay = () => {
    this.setState({
      currentOperand: this.getDisplayNumber(this.currentOperand),
      previousOperand: this.getDisplayNumber(this.previousOperand),
    });
    if (this.operation != null) {
      this.setState({
        previousOperand: `${this.getDisplayNumber(this.previousOperand)} ${
          this.operation
        }`,
      });
    } else {
      this.setState({
        previousOperand: ''
      })
    }
  }

  render() {
    return (
      <div className="calculator-grid">
        <ResultComponent
          previousOperand={this.state.previousOperand}
          currentOperand={this.state.currentOperand}
        />
        <KeyPadComponent
          number={this.number}
          operation={this.operationBtn}
          equals={this.equals}
          clear={this.clear}
          delete={this.delete}
        />
      </div>
    );
  }
}
