import React, {Component} from 'react';
import ResultComponent from './component/ResultComponent.js';
import KeyPadComponent from './component/KeyPadComponent.js';
import './App.css';

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      previousOperand: '',
      currentOperand: '',
      operation: undefined,
    };
  }

  number = (button) => {
    this.appendNumber(button);
  };

  operation = (button) => {
    this.chooseOperation(button);
  };

  equals = () => {
    this.compute();
  };

  clear = () => {
    this.setState({
      currentOperand: '',
      previousOperand: '',
      operation: undefined,
    });
  };

  delete = () => {
    this.setState({
      currentOperand: this.state.currentOperand.toString().slice(0, -1),
    });
  };

  appendNumber(number) {
    if (number === '.' && this.state.currentOperand.includes('.')) return;
    this.setState({
      currentOperand: this.state.currentOperand.toString() + number.toString(),
    });
  }

  chooseOperation = (operation) => {
    if (this.state.currentOperand === '') return;
    this.setState({
      operation: operation,
      previousOperand: this.state.currentOperand,
      currentOperand: '',
    });
    if (this.state.previousOperand !== '') {
      this.compute();
    }
  };

  compute = () => {
    let computation;
    const prev = parseFloat(this.state.previousOperand);
    const current = parseFloat(this.state.currentOperand);
    if (isNaN(prev) || isNaN(current)) return;
    switch (this.state.operation) {
      case '+':
        computation = prev + current;
        break;
      case '-':
        computation = prev - current;
        break;
      case '*':
        computation = prev * current;
        break;
      case '÷':
        computation = prev / current;
        break;
      case 'MAX':
        computation = Math.max(prev, current);
        break;
      case 'MIN':
        computation = Math.min(prev, current);
        break;
      default:
        return;
    }
    this.setState({
      currentOperand: computation,
      operation: undefined,
      previousOperand: '',
    });
  };

  updateDisplay = () => {};

  render() {
    return (
      <div className="calculator-grid">
        <ResultComponent
          previousOperand={this.state.previousOperand}
          currentOperand={this.state.currentOperand}
        />
        <KeyPadComponent
          number={this.number}
          operation={this.operation}
          equals={this.equals}
          clear={this.clear}
          delete={this.delete}
        />
      </div>
    );
  }
}
