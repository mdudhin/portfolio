import React, { Component } from 'react';

class ResultComponent extends Component {
    render() {
        let {previousOperand} = this.props;
        let {currentOperand} = this.props;
        return (
          <div class="output">
            <div className="previousOperand">{previousOperand}</div>
            <div className="currentOperand">{currentOperand}</div>
          </div>
        );
    }
}

export default ResultComponent;