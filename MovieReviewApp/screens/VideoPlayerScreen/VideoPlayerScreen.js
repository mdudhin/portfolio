import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {WebView} from 'react-native-webview';

export default function VideoPlayerScreen({route}) {
  const {video} = route.params;
  return (
    <WebView
      source={{
        uri:
          video,
      }}
    />
  );
}

const styles = StyleSheet.create({
  backgroundVideo: {
    flex: 1,
  },
});
