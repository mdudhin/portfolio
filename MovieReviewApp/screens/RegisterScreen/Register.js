import React, {useState, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  ToastAndroid,
  TouchableWithoutFeedback,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {Icon} from 'react-native-elements';
import RegisterForm from '../../components/RegisterForm';
import {useDispatch, useSelector} from 'react-redux';
import {handleRegister, handleResponse} from '../../redux/Action/AuthAction';

export default function Register({AnimationRef, onStateChange}) {
  const dispatch = useDispatch();
  const AuthState = useSelector((state) => state.auth);

  const [name, setName] = useState('');
  const [wrongName, setWrongName] = useState(false);
  const [trueName, setTrueName] = useState(false);

  const [email, setEmail] = useState('');
  const [wrongEmail, setWrongEmail] = useState(false);
  const [trueEmail, setTrueEmail] = useState(false);
  const [wrongEmailStatement, setWrongEmailStatement] = useState(false);

  const [password, setPassword] = useState('');
  const [wrongPassword, setWrongPassword] = useState(false);
  const [wrongPasswordStatement, setWrongPasswordStatement] = useState(false);
  const [secureTextEntry, setSecureTextEntry] = useState(true);

  const handleName = (val) => {
    setName(val);
    if (name != '') {
      setWrongName(false);
      setTrueName(true);
    }
  };

  const handleEmail = (val) => {
    setEmail(val);

    if (email != '') {
      setWrongEmail(false);
      setTrueEmail(true);
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (regex.test(email)) {
      setWrongEmail(false);
      setTrueEmail(true);
      setWrongEmailStatement('');
    } else {
      setWrongEmail(true);
      setTrueEmail(false);
      setWrongEmailStatement('Wrong email format');
    }
  };

  const handlePassword = (val) => {
    setPassword(val);

    if (password != '') {
      setWrongPassword(false);
    }

    const passwordLength = /^(?=.{6})/;
    const passwordNumber = /^(?=.*[0-9])/;

    if (!passwordLength.test(password)) {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be at least 6 character');
    } else if (!passwordNumber.test(password)) {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be at least 1 numeric');
    } else {
      setWrongPassword(false);
      setWrongPasswordStatement('');
    }
  };

  const updateSecureTextEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const register = async () => {
    if (name == '') {
      setWrongName(true);
      setTrueName(false);
    } else {
      setWrongName(false);
      setTrueEmail(true);
    }

    if (email == '') {
      setWrongEmail(true);
      setTrueEmail(false);
      setWrongEmailStatement('Email must be filled');
    } else {
      setWrongEmail(false);
      setTrueEmail(true);
      setWrongEmailStatement('');
    }

    if (password == '') {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be filled');
    } else {
      setWrongPassword(false);
      setWrongPasswordStatement('');
    }

    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const passwordLength = /^(?=.{6})/;
    const passwordNumber = /^(?=.*[0-9])/;

    if (regex.test(email)) {
      setWrongEmail(false);
      setTrueEmail(true);
      setWrongEmailStatement('');
    } else {
      setWrongEmail(true);
      setTrueEmail(false);
      setWrongEmailStatement('Wrong email format');
    }

    if (!passwordLength.test(password)) {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be at least 6 character');
    } else if (!passwordNumber.test(password)) {
      setWrongPassword(true);
      setWrongPasswordStatement('Password must be at least 1 numeric');
    } else {
      setWrongPassword(false);
      setWrongPasswordStatement('');
    }

    if (
      name != '' &&
      email != '' &&
      password != '' &&
      regex.test(email) &&
      passwordLength.test(password) &&
      passwordNumber.test(password)
    ) {
      console.log('Register berhasil', name, email, password);
      dispatch(handleRegister(name, email, password));
    } else {
      console.log('Register gagal');
    }
  };

  if (AuthState.isResponse) {
    ToastAndroid.show(AuthState.message, ToastAndroid.SHORT);
    dispatch(handleResponse());
  }

  return (
    <Animatable.View
      style={styles.content}
      animation="fadeInUp"
      ref={AnimationRef}>
      <TouchableWithoutFeedback onPress={onStateChange}>
        <Animatable.View style={styles.closeBtn}>
          <Icon name="x" type="feather" size={30} />
        </Animatable.View>
      </TouchableWithoutFeedback>
      <RegisterForm
        name={name}
        wrongName={wrongName}
        trueName={trueName}
        email={email}
        wrongEmail={wrongEmail}
        trueEmail={trueEmail}
        wrongEmailStatement={wrongEmailStatement}
        password={password}
        wrongPassword={wrongPassword}
        wrongPasswordStatement={wrongPasswordStatement}
        secureTextEntry={secureTextEntry}
        handleName={handleName}
        handleEmail={handleEmail}
        handlePassword={handlePassword}
        updateSecureTextEntry={updateSecureTextEntry}
        register={register}
        isLoading={AuthState.isLoading}
      />
    </Animatable.View>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  content: {
    height: height / 2.1,
    backgroundColor: 'white',
    marginLeft: 4,
    paddingHorizontal: 40,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  closeBtn: {
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    width: 50,
    height: 50,
    borderRadius: 30,
    marginTop: -25,
    marginBottom: 5,
  },
});
