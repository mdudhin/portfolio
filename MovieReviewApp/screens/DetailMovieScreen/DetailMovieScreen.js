import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  StatusBar,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ScrollView,
  TextInput,
  ToastAndroid,
} from 'react-native';
import {Icon} from 'react-native-elements';
import {useDispatch, useSelector} from 'react-redux';
import {
  getData,
  getReview,
  getActor,
  getCrew,
  clearData,
  addWatchList,
  clearResponse,
} from '../../redux/Action/DetailMovieAction';
import Actor from '../../components/Actor';
import Crew from '../../components/Crew';
import {addReview, handleResponse} from '../../redux/Action/ReviewMovieAction';
import {getWatchList} from '../../redux/Action/ShowMoviesAction';

export default function DetailMovieScreen({route, navigation}) {
  const {id} = route.params;
  const dispatch = useDispatch();
  const detailMovieState = useSelector((state) => state.detailMovie);
  const reviewMovieState = useSelector((state) => state.reviewMovie);
  const movie = detailMovieState.detailMovie;
  const review = detailMovieState.review;
  const actor = detailMovieState.actor;
  const crew = detailMovieState.crew;
  const average = (Number(detailMovieState.average) - 1) / 2;

  const image =
    Array.isArray(movie.Images) && movie.Images.length && movie.Images[3].url;
  const imageBackup =
    Array.isArray(movie.Images) && movie.Images.length && movie.Images[1].url;

  const video =
    Array.isArray(movie.Videos) && movie.Videos.length && movie.Videos[0].url;

  const genres =
    Array.isArray(movie.Genres) &&
    movie.Images.length &&
    movie.Genres.map((data, i) => (
      <Text style={{fontSize: 12}} key={i}>
        {movie.Genres.length - 1 == i ? `${data.name}` : `${data.name}, `}
      </Text>
    ));

  const [defaultRating, setDefaultRating] = useState(average);
  const [maxRating, setMaxRating] = useState(5);
  const [page, setPage] = useState();
  const [inputReview, setInputReview] = useState();

  useEffect(() => {
    dispatch(getData(id));
    dispatch(getReview(id, page, review));
    dispatch(getActor(id));
    dispatch(getCrew(id));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (reviewMovieState.isResponse) {
    ToastAndroid.show(reviewMovieState.message, ToastAndroid.SHORT);
    dispatch(handleResponse());
    dispatch(getData(id));
    dispatch(getReview(id, page, review));
    dispatch(getActor(id));
    dispatch(getCrew(id));
    navigation.navigate('MovieReview', {
      id: id,
    });
  }

  if (detailMovieState.isResponse) {
    ToastAndroid.show(detailMovieState.message, ToastAndroid.SHORT);
    dispatch(clearResponse());
    dispatch(getWatchList());
  }

  const addReviewData = () => {
    dispatch(addReview(id, inputReview, defaultRating));
  };

  const watchList = () => {
    dispatch(addWatchList(id));
  };

  const trailer = () => {
    navigation.navigate('VideoPlayer', {
      video: video,
    });
  };

  let rating = [];
  //Array to hold the filled or empty Stars
  for (var i = 1; i <= maxRating; i++) {
    rating.push(
      <TouchableOpacity activeOpacity={0.7} key={i}>
        <Image
          style={styles.StarImage}
          source={
            i <= average
              ? {
                  uri:
                    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                }
              : {
                  uri:
                    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                }
          }
        />
      </TouchableOpacity>,
    );
  }

  const backAndClear = () => {
    dispatch(clearData());
    navigation.goBack();
  };

  const handleReview = (val) => {
    setInputReview(val);
  };

  return (
    <ScrollView style={styles.container}>
      <StatusBar backgroundColor="transparent" translucent={true} />
      <View style={styles.header}>
        <Image
          source={
            image === 'http://image.tmdb.org/t/p/w1280null'
              ? {uri: `${imageBackup}`}
              : {
                  uri: `${image}`,
                }
          }
          style={styles.image}
        />
        <TouchableWithoutFeedback onPress={backAndClear}>
          <View style={styles.backButton}>
            <Icon name="arrow-left" type="feather" size={23} color="#fff" />
          </View>
        </TouchableWithoutFeedback>
        <TouchableOpacity style={styles.playButton} onPress={trailer}>
            <Icon name="play" type="font-awesome-5" size={20} color="#fff" />
        </TouchableOpacity>
      </View>
      <View style={styles.action}>
        <TouchableWithoutFeedback onPress={() => watchList()}>
          <Icon name="heart-o" type="font-awesome" size={25} color="#fff" />
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback>
          <Icon name="share-2" type="feather" size={25} color="#fff" />
        </TouchableWithoutFeedback>
      </View>
      <View style={styles.content}>
        <View style={styles.title}>
          <Text style={styles.titleText}>{movie.title}</Text>
        </View>
        <View style={styles.genre}>
          <Text style={styles.genreText}>{genres}</Text>
        </View>
        <View style={styles.rating}>
          {rating.map((data, i) => (
            <View key={i}>
              <Image
                style={styles.StarImage}
                source={
                  i <= average
                    ? {
                        uri:
                          'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                      }
                    : {
                        uri:
                          'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                      }
                }
              />
            </View>
          ))}
        </View>
        <View style={styles.synopsis}>
          <Text style={styles.synopsisText}>{movie.synopsis}</Text>
        </View>
        <View style={styles.actor}>
          <View style={styles.section}>
            <View style={styles.sectionTitle}>
              <Text style={styles.sectionText}>Actor</Text>
            </View>
            <TouchableOpacity style={styles.sectionAction}>
              <Icon name="arrowright" type="antdesign" size={25} color="#fff" />
            </TouchableOpacity>
          </View>
          <Actor actor={actor} />
        </View>
        <View style={styles.crew}>
          <View style={styles.section}>
            <View style={styles.sectionTitle}>
              <Text style={styles.sectionText}>Actor</Text>
            </View>
            <TouchableOpacity style={styles.sectionAction}>
              <Icon name="arrowright" type="antdesign" size={25} color="#fff" />
            </TouchableOpacity>
          </View>
          <Crew crew={crew} />
        </View>
        <View style={styles.addReview}>
          <View style={styles.addRating}>
            {rating.map((data, i) => (
              <TouchableOpacity onPress={() => setDefaultRating(i)} key={i}>
                <Image
                  style={styles.addStarImage}
                  source={
                    i <= defaultRating
                      ? {
                          uri:
                            'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                        }
                      : {
                          uri:
                            'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                        }
                  }
                />
              </TouchableOpacity>
            ))}
          </View>
          <View style={styles.formReview}>
            <View style={styles.actionForm}>
              <TextInput
                placeholder="Describe your experience"
                placeholderTextColor="rgba(17, 17, 17, 0.9)"
                autoCapitalize="none"
                multiline={true}
                numberOfLines={4}
                style={styles.textInput}
                onChangeText={(val) => handleReview(val)}
              />
            </View>
          </View>
          <TouchableOpacity
            onPress={() => addReviewData()}
            style={styles.btnAddReview}>
            <View style={styles.iconAddReview}>
              <Icon name="send" type="font-awesome" size={20} color="#fff" />
            </View>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('MovieReview', {
              id: id,
            })
          }
          style={styles.review}>
          <Text style={styles.reviewText}>View Review</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgb(12, 15, 17)',
  },
  content: {
    alignSelf: 'center',
    marginTop: 50,
  },
  header: {
    shadowColor: '#000',
    backgroundColor: '#fff',
    width: width,
    height: 370,
    borderBottomLeftRadius: width,
    borderBottomRightRadius: width,
    transform: [{scaleX: 1.8}],
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.48,
    shadowRadius: 14.27,
    elevation: 10,
  },
  image: {
    width: width,
    height: 370,
    borderBottomLeftRadius: width,
    borderBottomRightRadius: width,
    transform: [{scaleX: 1.3}],
    resizeMode: 'stretch',
  },
  backButton: {
    position: 'absolute',
    marginTop: 30,
    left: 100,
  },
  playButton: {
    position: 'absolute',
    backgroundColor: 'rgb(219, 0, 0)',
    justifyContent: 'center',
    paddingLeft: 3,
    width: 40,
    height: 45,
    borderRadius: 25,
    bottom: -22,
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 10,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  action: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
    marginBottom: -30,
    marginHorizontal: 40,
  },
  title: {
    marginHorizontal: 80,
    marginVertical: 7,
  },
  titleText: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'Montserrat-Medium',
    letterSpacing: 2,
    textAlign: 'center',
  },
  genre: {
    marginHorizontal: 90,
    marginVertical: 7,
  },
  genreText: {
    color: '#fff',
    fontSize: 12,
    fontFamily: 'Montserrat-Light',
    letterSpacing: 2,
    textAlign: 'center',
  },
  rating: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 8,
  },
  StarImage: {
    width: 23,
    height: 23,
    marginHorizontal: 1,
    resizeMode: 'cover',
  },
  synopsis: {
    marginHorizontal: 40,
    marginVertical: 7,
  },
  synopsisText: {
    color: '#fff',
    fontSize: 12,
    fontFamily: 'Montserrat-Light',
    letterSpacing: 1.5,
    lineHeight: 16,
    textAlign: 'justify',
  },
  actor: {
    height: 220,
    marginTop: 30,
    marginBottom: 20,
    marginHorizontal: 30,
  },
  crew: {
    height: 220,
    marginTop: 30,
    marginBottom: 20,
    marginHorizontal: 30,
  },
  review: {
    backgroundColor: 'rgb(219, 0, 0)',
    paddingHorizontal: 20,
    paddingVertical: 7,
    borderRadius: 20,
    marginTop: 60,
    marginBottom: 30,
    marginHorizontal: 30,
    alignSelf: 'center',
  },
  reviewText: {
    color: 'white',
    fontSize: 18,
    letterSpacing: 2,
  },
  section: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 25,
  },
  sectionTitle: {
    paddingLeft: 10,
  },
  sectionAction: {
    paddingRight: 10,
  },
  sectionText: {
    color: 'white',
    fontSize: 18,
    letterSpacing: 2,
  },
  addRating: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 15,
  },
  addStarImage: {
    width: 40,
    height: 40,
    marginHorizontal: 5,
    resizeMode: 'cover',
  },
  actionForm: {
    flexDirection: 'row',
    backgroundColor: '#F7F7F7',
    marginTop: 15,
    marginHorizontal: 40,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: '#F7F7F7',
    paddingLeft: 15,
    paddingRight: 15,
  },
  textInput: {
    flex: 1,
    paddingLeft: 12,
    paddingTop: 20,
    paddingBottom: 25,
    color: 'rgb(17, 17, 17)',
  },
  btnAddReview: {
    backgroundColor: 'rgb(219, 0, 0)',
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    marginVertical: -30,
    marginHorizontal: 30,
    justifyContent: 'center',
    alignSelf: 'flex-end',
  },
  iconAddReview: {
    marginLeft: -4,
  },
});
