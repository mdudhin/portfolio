import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import AllMovies from '../../components/AllMovies';
import {useDispatch, useSelector} from 'react-redux';
import {getMovieByGenre} from '../../redux/Action/ShowMoviesAction';

export default function ShowMovieByGenre({navigation, route}) {
  const dispatch = useDispatch();
  const showMoviesState = useSelector((state) => state.showMovies);
  const {genre} = route.params;

  useEffect(() => {
    dispatch(getMovieByGenre(genre));
  }, [dispatch, genre]);

  const handleLoadMore = () => {
    console.log('noLoad');
  };

  return (
    <View style={styles.container}>
      <AllMovies
        showMoviesState={showMoviesState.movieByGenre}
        handleLoadMore={handleLoadMore}
        navigation={navigation}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgb(12, 15, 17)',
    paddingHorizontal: 7,
  },
});
