import React, {useState, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import {Icon} from 'react-native-elements';
import LoginScreen from '../LoginScreen/Login';
import RegisterScreen from '../RegisterScreen/Register';

export default function Splash() {
  const [onChange, setOnChange] = useState(false);
  const [onScreen, setOnScreen] = useState('');
  const [onAnimated, setOnAnimated] = useState('fadeInUp');

  const onLogin = () => {
    setOnScreen('login');
    onStateChange();
  };

  const onRegister = () => {
    setOnScreen('register');
    onStateChange();
  };

  const onStateChange = () => {
    if (AnimationRef) {
      AnimationRef.current?.fadeOutDown();
    }
    setTimeout(() => {
      setOnChange(!onChange);
    }, 500);
  };

  const AnimationRef = useRef(null);

  const Screen = () => {
    if (onScreen == 'login') {
      return (
        <LoginScreen
          AnimationRef={AnimationRef}
          onStateChange={onStateChange}
        />
      );
    } else {
      return (
        <RegisterScreen
          AnimationRef={AnimationRef}
          onStateChange={onStateChange}
        />
      );
    }
  };

  return (
    <View style={{flex: 1}}>
      <StatusBar backgroundColor="transparent" translucent={true} />
      <View style={styles.container}>
        <View style={{...StyleSheet.absoluteFill}}>
          <Image
            source={require('../../assets/image/alexis-antoine-wdQlR2zTocU-unsplash.jpg')}
            style={styles.image}
          />
        </View>
        {onChange ? (
          <Screen />
        ) : (
          <Animatable.View style={styles.action} ref={AnimationRef}>
            <TouchableOpacity onPress={onLogin}>
              <Animatable.View
                style={styles.buttonLogin}
                animation={onAnimated}>
                <Text style={styles.textBtnLogin}>Sign In</Text>
              </Animatable.View>
            </TouchableOpacity>
            <TouchableOpacity onPress={onRegister}>
              <Animatable.View
                style={styles.buttonRegister}
                animation={onAnimated}>
                <Text style={styles.textBtnRegister}>Register</Text>
              </Animatable.View>
            </TouchableOpacity>
          </Animatable.View>
        )}
      </View>
    </View>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  image: {
    flex: 1,
    width: null,
    height: 100,
  },
  action: {
    height: height / 3.5,
  },
  buttonLogin: {
    backgroundColor: 'rgba(219, 0, 0, 0.9)',
    height: 50,
    marginHorizontal: 40,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 7,
  },
  buttonRegister: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    borderWidth: 1,
    borderColor: 'rgba(219, 0, 0, 1)',
    height: 50,
    marginHorizontal: 40,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 7,
  },
  textBtnLogin: {
    fontSize: 20,
    fontFamily: 'Montserrat-Light',
    color: 'white',
  },
  textBtnRegister: {
    fontSize: 20,
    fontFamily: 'Montserrat-Light',
    color: 'rgba(219, 0, 0, 1)',
  },
  closeBtn: {
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    width: 50,
    height: 50,
    borderRadius: 30,
    marginTop: -25,
  },
});
