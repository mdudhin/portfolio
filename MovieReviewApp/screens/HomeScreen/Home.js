import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {useDispatch} from 'react-redux';
import {handleLogout} from '../../redux/Action/AuthAction';
import Carousel from '../../components/Carousel';
import Genre from '../../components/Genre';
import UpComing from '../../components/UpComing';
import Movie from '../../components/Movie';
import {Icon} from 'react-native-elements';

export default function Home({navigation}) {
  const dispatch = useDispatch();
  const logout = () => {
    dispatch(handleLogout());
  };
  return (
    <ScrollView style={styles.container}>
      <StatusBar backgroundColor="transparent" translucent={true} />
      <View style={styles.header}>
        <Carousel navigation={navigation} />
      </View>
      <View style={styles.content}>
        <View style={styles.section}>
          <View style={styles.sectionTitle}>
            <Text style={styles.sectionText}>Genre</Text>
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate('AllGenre')}
            style={styles.sectionAction}>
            <Icon name="arrowright" type="antdesign" size={25} color="#fff" />
          </TouchableOpacity>
        </View>
        <Genre navigation={navigation} />
      </View>
      <View style={styles.content}>
        <View style={styles.section}>
          <View style={styles.sectionTitle}>
            <Text style={styles.sectionText}>Up Coming</Text>
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate('UpComingMovies')}
            style={styles.sectionAction}>
            <Icon name="arrowright" type="antdesign" size={25} color="#fff" />
          </TouchableOpacity>
        </View>
        <UpComing navigation={navigation} />
      </View>
      <View style={styles.content}>
        <View style={styles.section}>
          <View style={styles.sectionTitle}>
            <Text style={styles.sectionText}>Movie</Text>
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate('AllMovies')}
            style={styles.sectionAction}>
            <Icon name="arrowright" type="antdesign" size={25} color="#fff" />
          </TouchableOpacity>
        </View>
        <Movie navigation={navigation} />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgb(12, 15, 17)',
    paddingHorizontal: 7,
  },
  header: {
    marginTop: 20,
    marginBottom: 30,
  },
  content: {
    marginVertical: 30,
  },
  section: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 17,
  },
  sectionTitle: {
    paddingLeft: 10,
  },
  sectionAction: {
    paddingRight: 10,
  },
  sectionText: {
    color: 'white',
    fontSize: 18,
    letterSpacing: 2,
  },
});
