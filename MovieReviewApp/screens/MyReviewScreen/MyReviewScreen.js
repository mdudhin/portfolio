import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, ToastAndroid} from 'react-native';
import MyReview from '../../components/MyReview';
import {useDispatch, useSelector} from 'react-redux';
import {getMyReview} from '../../redux/Action/ReviewMovieAction';

export default function MyReviewScreen() {
  const dispatch = useDispatch();
  const reviewMovieState = useSelector((state) => state.reviewMovie);
  const review = reviewMovieState.myReview;

  useEffect(() => {
    dispatch(getMyReview());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.container}>
      <MyReview review={review} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgb(12, 15, 17)',
    paddingHorizontal: 7,
  },
});
