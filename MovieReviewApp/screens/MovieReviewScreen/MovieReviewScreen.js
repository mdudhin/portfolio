import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Review from '../../components/Review';
import {useDispatch, useSelector} from 'react-redux';
import {getReview} from '../../redux/Action/DetailMovieAction';

export default function MovieReviewScreen({route}) {
  const {id} = route.params;
  const dispatch = useDispatch();
  const detailMovieState = useSelector((state) => state.detailMovie);
  const review = detailMovieState.review;

  const [page, setPage] = useState(2);

  useEffect(() => {
    dispatch(getReview(id));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleLoadMore = () => {
    const pagePlus = page + 1;
    setPage(pagePlus);
    dispatch(getReview(id, page, review));
  };

  return (
    <View style={styles.container}>
      <Review review={review} id={id} handleLoadMore={handleLoadMore} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgb(12, 15, 17)',
    paddingHorizontal: 7,
  },
});
