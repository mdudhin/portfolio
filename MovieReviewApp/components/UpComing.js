import React, {useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  FlatList,
  Animated,
} from 'react-native';
import Item from './UpComingItem';
import {useDispatch, useSelector} from 'react-redux';
import {getUpComingMovies} from '../redux/Action/ShowMoviesAction';

export default function UpComing({navigation}) {
  const dispatch = useDispatch();
  const showUpComingMovieState = useSelector((state) => state.showMovies);

  useEffect(() => {
    dispatch(getUpComingMovies());
  }, [dispatch]);

  return (
    <View style={styles.container}>
      <FlatList
        data={showUpComingMovieState.upComingMovies}
        keyExtractor={(item) => `${item.id}`}
        horizontal
        scrollEnabled
        showsHorizontalScrollIndicator={false}
        renderItem={({item}) => {
          return <Item item={item} navigation={navigation} />;
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  carousel: {
    flex: 1,
  },
});
