import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  FlatList,
  Animated,
} from 'react-native';
import Item from './AllMoviesItem';

export default function AllMovies({
  showMoviesState,
  handleLoadMore,
  navigation,
}) {
  return (
    <View style={styles.container}>
      <FlatList
        data={showMoviesState}
        keyExtractor={(item) => `${item.id}`}
        columnWrapperStyle={{justifyContent: 'space-between'}}
        horizontal={false}
        numColumns={2}
        onEndReached={handleLoadMore}
        onEndReachedThreshold={0.5}
        initialNumToRender={5}
        renderItem={({item}) => {
          return <Item item={item} navigation={navigation} />;
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  carousel: {
    flex: 1,
  },
});
