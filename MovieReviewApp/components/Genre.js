import React, {useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  FlatList,
  Animated,
} from 'react-native';
import Item from './GenreItem';

const {height, width} = Dimensions.get('window');

const DATA = [
  {
    id: 28,
    name: 'Action',
    image: 'http://image.tmdb.org/t/p/w500/t93doi7EzoqLFckidrGGnukFPwd.jpg',
  },
  {
    id: 12,
    name: 'Adventure',
    image: 'http://image.tmdb.org/t/p/w500/a6cDxdwaQIFjSkXf7uskg78ZyTq.jpg',
  },
  {
    id: 16,
    name: 'Animation',
    image: 'http://image.tmdb.org/t/p/w500/hxt1WzpSqMMDVtjafAXAnMIb0o9.jpg',
  },
  {
    id: 35,
    name: 'Comedy',
    image: 'http://image.tmdb.org/t/p/w500/stmYfCUGd8Iy6kAMBr6AmWqx8Bq.jpg',
  },
  {
    id: 80,
    name: 'Crime',
    image: 'http://image.tmdb.org/t/p/w500/f5F4cRhQdUbyVbB5lTNCwUzD6BP.jpg',
  },
  {
    id: 14,
    name: 'Fantasy',
    image: 'http://image.tmdb.org/t/p/w500/xFxk4vnirOtUxpOEWgA1MCRfy6J.jpg',
  },
  {
    id: 10749,
    name: 'Romance',
    image: 'http://image.tmdb.org/t/p/w500/v4yVTbbl8dE1UP2dWu5CLyaXOku.jpg',
  },
  {
    id: 878,
    name: 'Science Fiction',
    image: 'http://image.tmdb.org/t/p/w500/5myQbDzw3l8K9yofUXRJ4UTVgam.jpg',
  },
];

export default function Genre({navigation}) {
  return (
    <FlatList
      data={DATA}
      keyExtractor={(item) => `${item.id}`}
      horizontal
      scrollEnabled
      showsHorizontalScrollIndicator={false}
      renderItem={({item}) => {
        return <Item item={item} navigation={navigation} />;
      }}
    />
  );
}
