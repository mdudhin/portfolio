import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, ToastAndroid, Image} from 'react-native';
import {ListItem, Icon} from 'react-native-elements';
import {useDispatch, useSelector} from 'react-redux';
import {handleLogout} from '../redux/Action/AuthAction';
import {
  DrawerContentScrollView,
  DrawerItemList,
} from '@react-navigation/drawer';
import {getUser} from '../redux/Action/ShowUserAction';

const DrawerContent = (props) => {
  const dispatch = useDispatch();
  const showUserState = useSelector((state) => state.showUser);

  useEffect(() => {
    dispatch(getUser());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const logout = () => {
    dispatch(handleLogout());
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image source={{uri: showUserState.user.image}} style={styles.image} />
        <Text style={styles.name}>{showUserState.user.name}</Text>
      </View>
      <DrawerContentScrollView {...props} style={styles.content}>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
      <ListItem
        title={<Text style={styles.title}>Log Out</Text>}
        containerStyle={{backgroundColor: '#1e272e'}}
        leftIcon={
          <Icon
            name="exit-to-app"
            type="materialicons"
            size={25}
            color="grey"
          />
        }
        onPress={logout}
      />
    </View>
  );
};

export default DrawerContent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1e272e',
  },
  header: {
    backgroundColor: 'rgb(12, 15, 17)',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: 250,
  },
  content: {
    paddingVertical: 10,
  },
  title: {
    textAlign: 'center',
    paddingRight: 20,
    letterSpacing: 2,
    color: 'grey',
  },
  image: {
    height: 110,
    width: 110,
    position: 'relative',
    borderRadius: 100,
    resizeMode: 'cover',
    marginVertical: 13,
  },
  name: {
    color: '#fff',
    fontSize: 14,
    letterSpacing: 2,
  },
});
