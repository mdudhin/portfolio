import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  ToastAndroid,
} from 'react-native';
import {Icon} from 'react-native-elements';
import Modal from 'react-native-modal';
import {
  editReview,
  handleResponse,
  getMyReview,
  deleteReview,
} from '../redux/Action/ReviewMovieAction';
import {useDispatch, useSelector} from 'react-redux';

export default function MyReviewItem({item}) {
  const dispatch = useDispatch();
  const reviewMovieState = useSelector((state) => state.reviewMovie);
  const [defaultRating, setDefaultRating] = useState((item.rating - 1) / 2);
  const [maxRating, setMaxRating] = useState(5);
  const [isModalVisible, setModalVisible] = useState(false);
  const [inputReview, setInputReview] = useState();

  if (reviewMovieState.isResponse) {
    ToastAndroid.show(reviewMovieState.message, ToastAndroid.SHORT);
    dispatch(handleResponse());
    setModalVisible(false);
    dispatch(getMyReview());
  }

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const handleReview = (val) => {
    setInputReview(val);
  };

  const image =
    Array.isArray(item.Movie.Images) &&
    item.Movie.Images.length &&
    item.Movie.Images[0].url;

  const ReviewDate = () => {
    const reviewDate = new Date(item.updatedAt);
    const dates = reviewDate.getDate();
    const month = reviewDate.getMonth();
    const year = reviewDate.getFullYear();
    return <Text style={styles.date}>{`${month + 1}/${dates}/${year}`}</Text>;
  };

  let rating = [];
  //Array to hold the filled or empty Stars
  for (var i = 1; i <= maxRating; i++) {
    rating.push(
      <TouchableOpacity activeOpacity={0.7} key={i}>
        <Image
          style={styles.StarImage}
          source={
            i <= (item.rating - 1) / 2
              ? {
                  uri:
                    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                }
              : {
                  uri:
                    'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                }
          }
        />
      </TouchableOpacity>,
    );
  }

  const edit = (id, comment, ratings) => {
    dispatch(editReview(id, comment, ratings));
    dispatch(getMyReview());
    setModalVisible(false);
  };

  const deleteData = (id) => {
    dispatch(deleteReview(id));
    dispatch(getMyReview());
  };

  return (
    <View style={styles.cardView}>
      <View style={styles.header}>
        <View style={{flexDirection: 'row', width: 200}}>
          <Image style={styles.image} source={{uri: image}} />
          <Text style={styles.name}>{item.Movie.title}</Text>
        </View>
        <View style={{alignSelf: 'center', flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() => deleteData(item.id)}
            style={{marginHorizontal: 5}}>
            <Icon name="trash" type="feather" size={17} color="#fff" />
          </TouchableOpacity>
          <TouchableOpacity onPress={toggleModal} style={{marginHorizontal: 5}}>
            <Icon name="edit" type="feather" size={17} color="#fff" />
          </TouchableOpacity>
          <Modal isVisible={isModalVisible}>
            <View style={styles.centeredView}>
              <View style={styles.addModal}>
                <TouchableWithoutFeedback onPress={toggleModal}>
                  <View style={styles.closeBtn}>
                    <Icon name="x" type="feather" size={30} />
                  </View>
                </TouchableWithoutFeedback>
                <Text style={styles.titleModal}>Edit Review</Text>
                <View style={styles.addRating}>
                  {rating.map((data, i) => (
                    <TouchableOpacity
                      onPress={() => setDefaultRating(i)}
                      key={i}>
                      <Image
                        style={styles.addStarImage}
                        source={
                          i <= defaultRating
                            ? {
                                uri:
                                  'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                              }
                            : {
                                uri:
                                  'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                              }
                        }
                      />
                    </TouchableOpacity>
                  ))}
                </View>
                <View style={styles.action}>
                  <Text style={styles.actionLabel}>Comment</Text>
                  <View style={styles.actionBody}>
                    <TextInput
                      placeholder="Describe your experience"
                      autoCapitalize="none"
                      multiline={true}
                      numberOfLines={3}
                      style={styles.textInput}
                      defaultValue={item.comment}
                      onChangeText={(val) => handleReview(val)}
                    />
                  </View>
                </View>
                <View style={styles.btnContainer}>
                  <TouchableOpacity
                    style={styles.btnSubmit}
                    onPress={() => edit(item.id, inputReview, defaultRating)}>
                    <Text style={styles.btnText}>Edit</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </View>
      </View>
      <View style={styles.content}>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.rating}>
            {rating.map((data, i) => (
              <View key={i}>
                <Image
                  style={styles.StarImage}
                  source={
                    i <= (item.rating - 1) / 2
                      ? {
                          uri:
                            'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_filled.png',
                        }
                      : {
                          uri:
                            'https://raw.githubusercontent.com/AboutReact/sampleresource/master/star_corner.png',
                        }
                  }
                />
              </View>
            ))}
          </View>
          <ReviewDate />
        </View>
        <Text style={styles.comment}>{item.comment}</Text>
      </View>
    </View>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  cardView: {
    flexDirection: 'column',
    // borderBottomWidth: 1,
    // borderColor: 'rgba(178, 190, 195, 0.7)',
    paddingVertical: 15,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  image: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    backgroundColor: '#EAEAEA',
  },
  name: {
    color: '#fff',
    fontSize: 13,
    fontFamily: 'Montserrat-Medium',
    textAlign: 'justify',
    alignSelf: 'center',
    marginHorizontal: 10,
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    marginVertical: 10,
  },
  comment: {
    color: '#fff',
    fontSize: 11,
    textAlign: 'justify',
    letterSpacing: 1,
    fontFamily: 'Montserrat-Light',
    marginTop: 7,
  },
  rating: {
    flexDirection: 'row',
    marginVertical: 3,
  },
  StarImage: {
    width: 12,
    height: 12,
    marginHorizontal: 1,
    resizeMode: 'cover',
  },
  date: {
    color: '#fff',
    fontSize: 11,
    letterSpacing: 1,
    fontFamily: 'Montserrat-Light',
    alignSelf: 'center',
    marginHorizontal: 10,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 50,
  },
  addModal: {
    backgroundColor: '#fff',
    margin: 20,
    borderRadius: 20,
    padding: 35,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  closeBtn: {
    alignSelf: 'flex-end',
    width: 40,
    height: 40,
    marginTop: -20,
  },
  addRating: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 15,
  },
  addStarImage: {
    width: 35,
    height: 35,
    marginHorizontal: 5,
    resizeMode: 'cover',
  },
  action: {
    flexDirection: 'column',
    marginTop: 15,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: 'grey',
    paddingTop: 15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  actionLabel: {
    marginTop: -5,
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  actionBody: {
    flexDirection: 'row',
    borderColor: 'grey',
    paddingTop: 7,
  },
  textInput: {
    flex: 1,
    marginTop: -12,
    paddingLeft: 12,
    color: '#05375a',
  },
  titleModal: {
    alignSelf: 'center',
    fontSize: 25,
    paddingBottom: 10,
  },
  btnText: {
    alignSelf: 'center',
    color: '#fff',
  },
  btnSubmit: {
    backgroundColor: 'rgb(219, 0, 0)',
    paddingVertical: 10,
    marginTop: 20,
    borderRadius: 30,
  },
});
