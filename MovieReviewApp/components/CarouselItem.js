import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

export default function CarouselItem({item, navigation}) {
  return (
    <View style={styles.cardView}>
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('DetailMovie', {
            id: item.id,
          })
        }>
        <Image style={styles.image} source={{uri: item.Images[1].url}} />
        {/* <Text style={styles.text}>{item.title}</Text> */}
      </TouchableOpacity>
    </View>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  cardView: {
    flex: 1,
  },
  image: {
    width: 200,
    height: height / 2.7,
    borderRadius: 20,
    alignSelf: 'center',
  },
  text: {
    color: '#fff',
    position: 'absolute',
    bottom: 10,
    left: 5,
  },
});
