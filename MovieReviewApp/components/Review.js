import React, {useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  FlatList,
  Animated,
  ScrollView,
} from 'react-native';
import Item from './ReviewItem';
import {getReview} from '../redux/Action/DetailMovieAction';
import {useDispatch} from 'react-redux';

export default function Review({review, handleLoadMore, id}) {
  const dispatch = useDispatch();
  return (
    <View style={styles.container}>
      <FlatList
        data={review}
        keyExtractor={(item) => `${item.id}`}
        refreshing={false}
        onRefresh={() => dispatch(getReview(id))}
        horizontal={false}
        onEndReached={handleLoadMore}
        onEndReachedThreshold={0.5}
        renderItem={({item}) => {
          return <Item item={item} />;
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 25,
  },
});
