import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

export default function WatchListItem({item, navigation}) {
  return (
    <View style={styles.cardView}>
      <TouchableOpacity
        onPress={() =>
          navigation.navigate('DetailWatchList', {
            id: item.Movie.id,
            watchListId: item.id,
          })
        }>
        <Image style={styles.image} source={{uri: item.Movie.Images[1].url}} />
      </TouchableOpacity>
    </View>
  );
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
  cardView: {
    flex: 1,
    marginHorizontal: 10,
    marginVertical: 10,
  },
  image: {
    width: 180,
    height: height / 3,
    borderRadius: 20,
    alignSelf: 'center',
  },
});
