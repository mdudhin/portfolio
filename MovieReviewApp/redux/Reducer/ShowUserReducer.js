import {
  GET_USER,
  FAILED_GET_USER,
  EDIT_USER,
  FAILED_EDIT_USER,
  SHOW_LOADING,
  CLEAR_RESPONSE,
} from '../Type/showUser';

const initialState = {
  user: [],
  message: null,
  isResponse: false,
  isLoading: false,
};

export const ShowUserReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER:
      return {
        ...state,
        user: action.user,
        isResponse: true,
      };
    case FAILED_GET_USER:
      return {
        ...state,
        message: action.message,
        isResponse: true,
      };
    case EDIT_USER:
      return {
        ...state,
        message: action.message,
        isResponse: true,
        isLoading: false,
      };
    case FAILED_EDIT_USER:
      return {
        ...state,
        message: action.message,
        isResponse: true,
        isLoading: false,
      };
    case SHOW_LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case CLEAR_RESPONSE:
      return {
        ...state,
        message: null,
        isResponse: false,
      };
    default:
      return state;
  }
};
