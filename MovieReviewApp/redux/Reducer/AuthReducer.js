import {
  REQUEST_LOGIN,
  CHECK_TOKEN,
  REGISTER,
  FAILED_LOGIN,
  FAILED_REGISTER,
  SHOW_LOADING,
  HIDE_RESPONSE,
} from '../Type/auth';

const initialState = {
  isLoading: false,
  isResponse: false,
  token: null,
  email: null,
  message: null,
};

export const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHECK_TOKEN:
      return {
        ...state,
        token: action.token,
      };
    case HIDE_RESPONSE:
      return {
        ...state,
        isResponse: false,
      };
    case SHOW_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case REQUEST_LOGIN:
      return {
        ...state,
        token: action.token,
        email: action.email,
        message: action.message,
        isLoading: false,
        isResponse: true,
      };
    case REGISTER:
      return {
        ...state,
        token: action.token,
        email: action.email,
        message: action.message,
        isLoading: false,
        isResponse: true,
      };
    case FAILED_LOGIN:
      return {
        ...state,
        message: action.message,
        isLoading: false,
        isResponse: true,
      };
    case FAILED_REGISTER:
      return {
        ...state,
        message: action.message,
        isLoading: false,
        isResponse: true,
      };

    default:
      return state;
  }
};
