import {
  GET_DATA,
  FAILED_GET_DATA,
  GET_REVIEW,
  FAILED_GET_REVIEW,
  GET_ACTOR,
  FAILED_GET_ACTOR,
  GET_CREW,
  FAILED_GET_CREW,
  CLEAR_DATA,
  ADD_WATCH_LIST,
  FAILED_ADD_WATCH_LIST,
  DELETE_WATCH_LIST,
  FAILED_DELETE_WATCH_LIST,
  CLEAR_RESPONSE,
} from '../Type/detailMovie';

const initialState = {
  detailMovie: [],
  review: [],
  actor: [],
  crew: [],
  average: null,
  message: null,
};

export const DetailMovieReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_DATA:
      return {
        ...state,
        detailMovie: action.data,
      };
    case FAILED_GET_DATA:
      return {
        ...state,
        message: action.message,
      };
    case GET_REVIEW:
      return {
        ...state,
        review: action.review,
        average: action.average,
      };
    case FAILED_GET_REVIEW:
      return {
        ...state,
        message: action.message,
      };
    case GET_ACTOR:
      return {
        ...state,
        actor: action.actor,
      };
    case FAILED_GET_ACTOR:
      return {
        ...state,
        message: action.message,
      };
    case GET_CREW:
      return {
        ...state,
        crew: action.crew,
      };
    case FAILED_GET_CREW:
      return {
        ...state,
        message: action.message,
      };
    case ADD_WATCH_LIST:
      return {
        ...state,
        message: action.message,
        isResponse: true,
      };
    case FAILED_ADD_WATCH_LIST:
      return {
        ...state,
        message: action.message,
        isResponse: true,
      };
    case DELETE_WATCH_LIST:
      return {
        ...state,
        message: action.message,
        isResponse: true,
      };
    case FAILED_DELETE_WATCH_LIST:
      return {
        ...state,
        message: action.message,
        isResponse: true,
      };
    case CLEAR_RESPONSE:
      return {
        ...state,
        message: null,
        isResponse: false,
      };
    case CLEAR_DATA:
      return {
        ...state,
        detailMovie: [],
        review: [],
        actor: [],
        crew: [],
        average: null,
        message: null,
      };
    default:
      return state;
  }
};
