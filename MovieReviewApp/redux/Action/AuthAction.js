import Axios from 'axios';
import {
  REQUEST_LOGIN,
  CHECK_TOKEN,
  REGISTER,
  FAILED_LOGIN,
  FAILED_REGISTER,
  SHOW_LOADING,
  HIDE_RESPONSE,
} from '../Type/auth';
import AsyncStorage from '@react-native-community/async-storage';

export const handleResponse = () => {
  return async (dispatch) => {
    dispatch({type: HIDE_RESPONSE});
  };
};

export const handleCheckToken = () => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      dispatch({type: CHECK_TOKEN, token: token});
    } catch (error) {
      console.log(error);
    }
  };
};

export const handleLogin = (email, password) => {
  return async (dispatch) => {
    dispatch({type: SHOW_LOADING, isLoading: true});
    try {
      const res = await Axios.post(
        'https://ga-moviereview.herokuapp.com/api/v1/user/login',
        {
          email: email,
          password: password,
        },
      );
      console.log(res.data.data.token);
      const token = res.data.data.token;
      await AsyncStorage.setItem('@token', token);
      const message = 'Success to login';
      dispatch({
        type: REQUEST_LOGIN,
        token: token,
        email: email,
        message: message,
      });
    } catch (e) {
      const error = e.response.data.message;
      console.log(error);
      dispatch({type: FAILED_LOGIN, message: e.response.data.message});
    }
  };
};

export const handleRegister = (name, email, password) => {
  return async (dispatch) => {
    dispatch({type: SHOW_LOADING, isLoading: true});
    try {
      const res = await Axios.post(
        'https://ga-moviereview.herokuapp.com/api/v1/user/register',
        {
          name: name,
          email: email,
          password: password,
        },
      );
      const token = res.data.data.token;
      console.log(token);
      await AsyncStorage.setItem('@token', token);
      const message = 'Success to login';
      dispatch({type: REGISTER, token: token, email: email, message: message});
    } catch (e) {
      dispatch({type: FAILED_REGISTER, message: e.response.data.message});
    }
  };
};

export const handleLogout = () => {
  return async (dispatch) => {
    try {
      await AsyncStorage.clear();
      dispatch({type: REQUEST_LOGIN, token: null, email: null});
    } catch (error) {
      console.log(error);
    }
  };
};
