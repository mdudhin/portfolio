import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {
  ADD_REVIEW,
  FAILED_ADD_REVIEW,
  HIDE_RESPONSE_REVIEW,
  GET_MY_REVIEW,
  FAILED_GET_MY_REVIEW,
  EDIT_REVIEW,
  FAILED_EDIT_REVIEW,
  DELETE_REVIEW,
  FAILED_DELETE_REVIEW,
} from '../Type/reviewMovie';

export const handleResponse = () => {
  return (dispatch) => {
    dispatch({type: HIDE_RESPONSE_REVIEW});
  };
};

export const addReview = (id, inputReview, defaultRating) => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = axios.post(
        `https://ga-moviereview.herokuapp.com/api/v1/review/new/${id}`,
        {
          comment: inputReview,
          rating: (defaultRating + 1) * 2,
        },
        {
          headers: {
            auth: token,
          },
        },
      );
      if (res !== null) {
        const message = 'Success to add review !';
        dispatch({
          type: ADD_REVIEW,
          message: message,
        });
      }
    } catch (e) {
      const error = e.response.data.message;
      console.log(error);
      dispatch({type: FAILED_ADD_REVIEW, message: e.response.data.message});
    }
  };
};

export const editReview = (id, comment, ratings) => {
  return async (dispatch) => {
    console.log(id, comment, ratings);
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = axios.put(
        `https://ga-moviereview.herokuapp.com/api/v1/review/${id}`,
        {
          comment: comment,
          rating: (ratings + 1) * 2,
        },
        {
          headers: {
            auth: token,
          },
        },
      );
      if (res !== null) {
        const message = 'Success to edit review !';
        dispatch({
          type: EDIT_REVIEW,
          message: message,
        });
      }
    } catch (e) {
      const error = e.response.data.message;
      console.log(error);
      dispatch({type: FAILED_EDIT_REVIEW, message: e.response.data.message});
    }
  };
};

export const deleteReview = (id) => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = axios.delete(
        `https://ga-moviereview.herokuapp.com/api/v1/review/${id}`,
        {
          headers: {
            auth: token,
          },
        },
      );
      if (res !== null) {
        const message = 'Success to delete review !';
        dispatch({
          type: DELETE_REVIEW,
          message: message,
        });
      }
    } catch (e) {
      const error = e.response.data.message;
      console.log(error);
      dispatch({type: FAILED_DELETE_REVIEW, message: e.response.data.message});
    }
  };
};

export const getMyReview = () => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = await axios.get(
        'https://ga-moviereview.herokuapp.com/api/v1/review/owned',
        {
          headers: {
            auth: token,
          },
        },
      );
      if (res !== null) {
        const data = res.data.data.rows;
        dispatch({
          type: GET_MY_REVIEW,
          myReview: data,
        });
      }
    } catch (e) {
      dispatch({type: FAILED_GET_MY_REVIEW, message: e.response.data.message});
    }
  };
};
