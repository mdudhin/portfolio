import axios from 'axios';
import {
  GET_DATA,
  FAILED_GET_DATA,
  GET_REVIEW,
  FAILED_GET_REVIEW,
  GET_ACTOR,
  FAILED_GET_ACTOR,
  GET_CREW,
  FAILED_GET_CREW,
  CLEAR_DATA,
  ADD_WATCH_LIST,
  FAILED_ADD_WATCH_LIST,
  DELETE_WATCH_LIST,
  FAILED_DELETE_WATCH_LIST,
  CLEAR_RESPONSE,
} from '../Type/detailMovie';
import AsyncStorage from '@react-native-community/async-storage';

export const getData = (id) => {
  return async (dispatch) => {
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = await axios.get(
        `https://ga-moviereview.herokuapp.com/api/v1/movie/${id}`,
      );
      if (res !== null) {
        const data = res.data.data.detail;
        dispatch({
          type: GET_DATA,
          data: data,
        });
      }
    } catch (e) {
      const error = e.response.data.message;
      console.log(error);
      dispatch({type: FAILED_GET_DATA, message: e.response.data.message});
    }
  };
};

export const getReview = (id, page, review) => {
  return async (dispatch) => {
    try {
      const res = await axios.get(
        `https://ga-moviereview.herokuapp.com/api/v1/review/${id}/?limit=10&page=${
          page === undefined ? 1 : page + 1
        }`,
      );
      if (res !== null) {
        const data = res.data.data;
        const reviewData = data.review.rows;
        dispatch({
          type: GET_REVIEW,
          review: page === undefined ? reviewData : [...review, ...reviewData],
          average: data.average,
        });
      }
    } catch (e) {
      dispatch({type: FAILED_GET_REVIEW, message: e.response.data.message});
    }
  };
};

export const getActor = (id) => {
  return async (dispatch) => {
    try {
      const res = await axios.get(
        `https://ga-moviereview.herokuapp.com/api/v1/movie/${id}/actor`,
      );
      if (res !== null) {
        const data = res.data.data;
        dispatch({
          type: GET_ACTOR,
          actor: data,
        });
      }
    } catch (e) {
      const error = e.response.data.message;
      console.log(error);
      dispatch({type: FAILED_GET_ACTOR, message: e.response.data.message});
    }
  };
};

export const getCrew = (id) => {
  return async (dispatch) => {
    try {
      const res = await axios.get(
        `https://ga-moviereview.herokuapp.com/api/v1/movie/${id}/crew`,
      );
      if (res !== null) {
        const data = res.data.data;
        dispatch({
          type: GET_CREW,
          crew: data,
        });
      }
    } catch (e) {
      const error = e.response.data.message;
      console.log(error);
      dispatch({type: FAILED_GET_CREW, message: e.response.data.message});
    }
  };
};

export const addWatchList = (id) => {
  return async (dispatch) => {
    console.log(id);
    try {
      const token = await AsyncStorage.getItem('@token');
      axios
        .post(
          `https://ga-moviereview.herokuapp.com/api/v1/movie/watchlist/${id}`,
          {},
          {
            headers: {
              auth: token,
            },
          },
        )
        .then((res) => {
          if (res.data.status == 'success') {
            const message = 'Success to add watch list !';
            dispatch({
              type: ADD_WATCH_LIST,
              message: message,
            });
          } else {
            dispatch({
              type: FAILED_ADD_WATCH_LIST,
              message: res.data.message,
            });
          }
        })
        .catch((error) => {
          console.log(error);
          dispatch({
            type: FAILED_ADD_WATCH_LIST,
            message: 'You already add this movie to watch list!',
          });
        });
    } catch (e) {
      console.log(e);
    }
  };
};

export const deleteWatchList = (id) => {
  return async (dispatch) => {
    console.log(id);
    try {
      const token = await AsyncStorage.getItem('@token');
      const res = axios.delete(
        `https://ga-moviereview.herokuapp.com/api/v1/movie/watchlist/${id}`,
        {
          headers: {
            auth: token,
          },
        },
      );
      if (res !== null) {
        const message = 'Success to delete !';
        dispatch({
          type: DELETE_WATCH_LIST,
          message: message,
        });
      }
    } catch (e) {
      const error = e.response.data.message;
      console.log(error);
      dispatch({
        type: FAILED_DELETE_WATCH_LIST,
        message: e.response.data.message,
      });
    }
  };
};

export const clearResponse = () => {
  return (dispatch) => {
    dispatch({type: CLEAR_RESPONSE});
  };
};

export const clearData = () => {
  return (dispatch) => {
    dispatch({
      type: CLEAR_DATA,
    });
  };
};
